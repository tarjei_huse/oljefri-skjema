import React from 'react'
// import TestUtils from 'react-addons-test-utils'
import { findDOMNode } from 'react-dom'
import 'whatwg-fetch'
// import qs from 'qs'

import { mount } from 'enzyme'

import { SchemaRoot } from './SchemaRoot'

import setValue from './setValue'
import {
  fetchServicesFlatResponse,
  providerQueryResponse,
  // sendRequestOKResponse,
} from './fixtures'

// import lang from './lang'
import sinon from 'sinon'

// import Page1 from './Page1'
import Page2, { Page2NoProviders } from './Page2'
import Page3 from './Page3'
import Page4 from './Page4'
import Page5 from './Page5'
import Page6 from './Page6'

import fetchMock from 'fetch-mock'

describe('SchemaRoot', function () {
  function setup(props) {
    return mount(<SchemaRoot {...props} />)
  }

  /*
  function mockPostnumberCheck() {
    mocks = fetchMock.mock(/.+frontendLookups\/checkPostcode\?postnummer/, {
      poststed: 'Oslo',
      hasProviders: true,
      providerCount: '10',
      activeKommune: true,
    })

    mocks.mock(
      /.+\/sf\/bolig\/fetchServiceGroups\?postnummer=0575\&market=private/,
      fetchServicesFlatResponse()
    )
  }
*/
  function nextIsDisabled(page, expected = true) {
    expect(page.find('.next-page').props().disabled).toEqual(expected)
  }
  afterEach(function () {
    fetchMock.restore()
  })

  describe('Page1', function () {
    function makeProps(page) {
      return {
        page,
        site: 'oljefri',
        fields: {},
        setPage: sinon.spy(),
        setField: sinon.spy(),
      }
    }

    it('should render the postnr field', function () {
      let props = makeProps(1)
      let page = setup(props)

      expect(page.find('input').length).toBe(1)

      let node = findDOMNode(page.instance())

      let input = node.querySelector('input')

      expect(input.name).toEqual('postnummer')

      input.value = '0575'
      expect(page.find('button').find('.button').props().disabled).toBe(true)

      expect(page.find('.postnumber').length).toBe(1)

      page.find('.postnumber').simulate('change')

      expect(props.setField.called).toBe(true)
      expect(props.setField.args[0]).toEqual(['postnummer', '0575'])

      page.setProps({ fields: { postnummer: '0575' } })

      expect(page.find('button').find('.button').first().props().disabled).toBe(
        false
      )

      page.find('.button').simulate('click')

      expect(props.setPage.called).toBe(true)

      expect(props.setPage.args[0][0]).toEqual('2')
    })
  })

  describe('Page2', function () {
    function makeProps() {
      return {
        site: 'oljefri',
        page: 2,
        postnummer: {
          hasProviders: true,
          providerCount: 10,
        },
        services: [],
        selectedServices: {},
        fields: {},

        setPage: sinon.spy(),
        selectMany: sinon.spy(),
        setField: sinon.spy(),
        select: sinon.spy(),
      }
    }

    it('should be able to render page 2 while loading serviceGroups', function () {
      let props = makeProps()
      let page = setup(props)
      expect(page.find(Page2).length).toBe(1)
    })

    it.skip('should trigger a gtm event on page change', function () {
      let props = makeProps()
      props.services = fetchServicesFlatResponse()

      let page = setup(props)

      let input = page.find('.service-type-checkbox').first()

      setValue(input, false, true)

      expect(props.select.called).toBe(true)
      expect(props.select.args[0]).toEqual(['service', 9])

      page.setProps({ selectedServices: { 14: true } })

      page.find('.next-page').simulate('click')

      expect(props.setPage.called).toBe(true)
      console.error(window.dataLayer[0])

      expect(window.dataLayer.length).toEqual(1)

      expect(window.dataLayer[0]).toEqual({
        host: 'localhost',
        page: 2,
        event: 'oljefri.schema.pagechange',
      })
    })

    it('should handle selecting serviceGroups', function () {
      let props = makeProps()
      props.services = fetchServicesFlatResponse()

      let page = setup(props)

      expect(page.find(Page2).length).toBe(1)

      page.find('.next-page').simulate('click')
      expect(props.setPage.called).toBe(false)

      expect(page.find('.Page2-must-select').length).toBeGreaterThan(0)

      expect(page.find('.Page2-service-type').length).toBe(15)
      expect(page.find('.service-type-checkbox').length).toBe(15)

      let input = page.find('.service-type-checkbox').first()

      setValue(input, false, true)

      expect(props.select.called).toBe(true)
      expect(props.select.args[0]).toEqual(['service', 9])

      page.find('.next-page').simulate('click')
      expect(props.setPage.called).toBe(false)

      page.setProps({ selectedServices: { 14: true } })

      page.find('.next-page').simulate('click')
      expect(props.setPage.called).toBe(true)

      expect(
        page.find('input').filterWhere((elem) => {
          return elem.props().checked
        }).length
      ).toBe(1)

      page.find('.next-page').simulate('click')

      expect(props.setPage.called).toBe(true)
      expect(props.setPage.args[0][0]).toEqual('3')
    })

    it('should handle selecting serviceGroups on energismart', function () {
      let props = makeProps()
      props.site = 'energismart'
      props.services = fetchServicesFlatResponse()

      let page = setup(props)

      expect(page.find(Page2).length).toBe(1)

      page.find('.next-page').simulate('click')
      expect(props.setPage.called).toBe(false)

      expect(page.find('.Page2-must-select').length).toBeGreaterThan(0)

      expect(page.find('.Page2-service-type').length).toBeGreaterThan(4)
      expect(page.find('.service-type-checkbox').length).toBeGreaterThan(4)

      let input = page.find('.service-type-checkbox').first()

      setValue(input, false, true)

      expect(props.select.called).toBe(true)
      expect(props.select.args[0]).toEqual(['service', 13])

      page.find('.next-page').simulate('click')
      expect(props.setPage.called).toBe(false)

      page.setProps({ selectedServices: { 13: true } })

      page.find('.next-page').simulate('click')
      expect(props.setPage.called).toBe(true)

      expect(
        page.find('input').filterWhere((elem) => {
          return elem.props().checked
        }).length
      ).toBe(1)

      page.find('.next-page').simulate('click')

      expect(props.setPage.called).toBe(true)
      expect(props.setPage.args[0][0]).toEqual('3')
    })

    it('should handle select all serviceGroups', function () {
      let props = makeProps()
      props.services = fetchServicesFlatResponse()

      let page = setup(props)

      expect(page.find(Page2).length).toBe(1)

      page.find('.SelectAllButton').simulate('click')
      expect(props.selectMany.called).toBe(true)
    })

    it('should show the invalid page if postnr is not defined', function () {
      let props = makeProps()
      props.postnummer = {
        poststed: 'Troms\u00f8',
        hasProviders: false,
        providerCount: '0',
        activeKommune: true,
      }
      let page = setup(props)
      expect(page.find(Page2NoProviders).length).toBe(1)
    })
  })

  describe('Page3', function () {
    function makeProps() {
      let fields = {}
      return {
        site: 'energismart',
        page: 3,
        postnummer: {
          hasProviders: true,
          providerCount: 10,
        },
        serviceGroups: fetchServicesFlatResponse(),
        selectedServiceGroups: { 5: true },
        fields: fields,
        setPage: sinon.spy(),
        setField: function (f, v) {
          fields[f] = v
        },

        select: sinon.spy(),
      }
    }
    it('should handle buildingtype inputs', function () {
      let props = makeProps()
      let page = setup(props)

      expect(page).toBeDefined()

      expect(page.find(Page3).length).toBe(1)

      page.find('.next-page').simulate('click')
      expect(page.props().setPage.called).toBe(false)

      setValue(page, '.building-type', '1')
      setValue(page, '.year-built', '1920')
      setValue(page, '.comment', 'c')
      page.setProps({ fields: props.fields })

      page.find('.next-page').simulate('click')
      expect(page.props().setPage.called).toBe(false)

      setValue(page, '.area', '120')
      page.setProps({ fields: props.fields })

      nextIsDisabled(page, false)

      let fields = props.fields
      expect(fields['buildingType_id']).toEqual('1')
      expect(fields['yearBuilt']).toEqual('1920')
      expect(fields['area']).toEqual('120')
      expect(fields['comment']).toEqual('c')
    })
  })

  describe('Page4', function () {
    function makeProps() {
      let fields = {}
      return {
        page: 4,
        site: 'energismart',
        postnummer: {
          hasProviders: true,
          providerCount: 10,
        },
        serviceGroups: fetchServicesFlatResponse(),
        selectedServiceGroups: { 5: true },
        toggledProviders: {},
        selectedProviders: {},
        providers: providerQueryResponse(),
        fields: fields,
        setPage: sinon.spy(),
        setField: function (f, v) {
          fields[f] = v
        },
        select: sinon.spy(),
      }
    }
    it('should list out all the providers that are available', function () {
      let props = makeProps()
      let page = setup(props)

      expect(page).toBeDefined()

      expect(page.find(Page4).length).toBe(1)

      expect(page.find('.Provider').length).toBe(providerQueryResponse().length)

      nextIsDisabled(page)

      page
        .find('.Provider')
        .find('input')
        .first()
        .simulate('change', {
          target: {
            value: 45,
            checked: true,
          },
        })

      expect(props.select.args[0]).toEqual(['selectedProvider', 45])
      page.setProps({ selectedProviders: { 45: true } })

      expect(page.find('.provider-19').find('.callout').length).toBe(0)

      /*
      page
        .find('.provider-19')
        .find('.expand')
        .simulate('click')

      expect(props.select.args[1]).toEqual(['toggledProvider', '19'])
      page.setProps({ toggledProviders: { '19': true } })

      expect(page.find('.provider-19').find('.callout').length).toBe(1)
*/
      nextIsDisabled(page, false)
    })
  })

  describe('Page5', function () {
    function makeProps() {
      let fields = {}
      return {
        site: 'energismart',
        page: 5,
        postnummer: {
          hasProviders: true,
          providerCount: 10,
        },
        serviceGroups: fetchServicesFlatResponse(),
        selectedServiceGroups: { 5: true },
        toggledProviders: {},
        selectedProviders: { 5: true },
        providers: providerQueryResponse(),
        fields: fields,
        setPage: sinon.spy(),
        setField: function (f, v) {
          fields[f] = v
        },
        select: sinon.spy(),
      }
    }
    it('should let the user fill inn the form', function () {
      let props = makeProps()
      let page = setup(props)

      expect(page).toBeDefined()

      expect(page.find(Page5).length).toBe(1)

      nextIsDisabled(page, true)

      setValue(page, '#firstname', 'Haralt')
      setValue(page, '#lastname', 'Pra')
      setValue(page, '#adress', 'Got 1')
      setValue(page, '#postnr', '9899')
      setValue(page, '#postplace', 'Alta')
      setValue(page, '#phone', '92039020')
      setValue(page, '#email', 'ema@google.com')
      setValue(page, '#wantsToBeMember', true)
      setValue(page, '#termsAgreed', true)

      page.setProps({ fields: props.fields })

      nextIsDisabled(page, false)
    })

    it('should show validation info if input is missing', function () {
      let props = makeProps()
      let page = setup(props)

      expect(page).toBeDefined()

      expect(page.find(Page5).length).toBe(1)

      nextIsDisabled(page, true)

      page.find('.next-page').simulate('click')
    })
  })

  describe('Page6', function () {
    function makeProps() {
      let fields = {}
      return {
        page: 6,
        site: 'energismart',
        postnummer: {
          hasProviders: true,
          providerCount: 10,
        },
        serviceGroups: fetchServicesFlatResponse(),
        selectedServiceGroups: { 5: true },
        toggledProviders: {},
        selectedProviders: { 5: true },
        providers: providerQueryResponse(),
        fields: fields,
        setPage: sinon.spy(),
        setField: function (f, v) {
          fields[f] = v
        },
        select: sinon.spy(),

        requestResult: {
          request: {
            id: '1486',
            firms: [
              ['25', 'Holtefjell Energi AS'],
              ['27', 'Arna og \u00c5sane R\u00f8rleggerservice AS'],
            ],
          },
        },
      }
    }

    it('should render', function () {
      let props = makeProps()
      let page = setup(props)
      expect(page).toBeDefined()

      expect(page.find(Page6).length).toBe(1)

      page.find('.provider-logo img').forEach((img) => {
        const path = img.props().src
        expect(path).toContain('https://')
        expect(path.split('https').length).toBe(2)
      })
    })
  })
})
