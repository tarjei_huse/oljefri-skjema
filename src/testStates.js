function p2() {
  return {
    fields: {
      postnummer: '5010',
      postPostcode: '5010',
      postPostplace: 'Bergen',
    },
    page: 2,
    postnummer: {
      providerCount: 11,
      hasProviders: true,
      poststed: 'Bergen',
      activeKommune: true,
    },
    serviceGroups: {
      groups: [
        {
          id: '4',
          name: 'Ved',
          comment: null,
          helpText: null,
          grouping: 'heating',
          Services: [
            {
              id: '3',
              name: 'Vedovn med vannkappe',
              comment: '',
              helpText:
                'Varmer opp vann til vannbåren varme i tillegg til rommet vedovnen står i.',
              active: true,
              servicegroup_id: '4',
            },
            {
              id: '5',
              name: 'Vedovn, peis eller innsats',
              comment: '',
              helpText: 'Nye vedovner forurenser mindre, og er mer effektive.',
              active: true,
              servicegroup_id: '4',
            },
            {
              id: '23',
              name: 'Vedkjel',
              comment: '',
              helpText: '',
              active: true,
              servicegroup_id: '4',
            },
          ],
        },
        {
          id: '7',
          name: 'Rens og fjerning av oljetank',
          comment: null,
          helpText: null,
          grouping: 'oiltank',
          Services: [
            {
              id: '9',
              name: 'Oljetanksanering',
              comment: '',
              helpText: 'Oljetanken tømmes for restolje, renses og fjernes.',
              active: true,
              servicegroup_id: '7',
            },
          ],
        },
        {
          id: '5',
          name: 'Solenergi',
          comment: null,
          helpText: null,
          grouping: 'heating',
          Services: [
            {
              id: '11',
              name: 'Solceller',
              comment: '',
              helpText:
                'Omdanner energien fra solinnstrålingen til elektrisitet.',
              active: true,
              servicegroup_id: '5',
            },
            {
              id: '17',
              name: 'Solfangere',
              comment: '',
              helpText:
                'Solfangere på tak eller fasade varmer opp vann som kan brukes til oppvarming av boarealer eller tappevann. ',
              active: true,
              servicegroup_id: '5',
            },
          ],
        },
        {
          id: '1',
          name: 'Varmepumpe',
          comment: '',
          helpText: '',
          grouping: 'heating',
          Services: [
            {
              id: '13',
              name: 'Luft-luft varmepumpe',
              comment: '',
              helpText:
                'Henter varmeenergi fra uteluften, hever temperaturen og sprer varmen i bygget ved hjelp av en vifte.',
              active: true,
              servicegroup_id: '1',
            },
            {
              id: '14',
              name: 'Luft-vann varmepumpe',
              comment: '',
              helpText:
                'Henter energi fra uteluften, hever temperaturen og avgir varme i bygget som vannbåren varme.',
              active: true,
              servicegroup_id: '1',
            },
            {
              id: '15',
              name: 'Berg-, sjø- eller jordvarmepumpe',
              comment: '',
              helpText:
                'Henter energi fra sjø, berggrunn eller jordsmonn, hever temperaturen og avgir varmen til bygget som vannbåren varme.',
              active: true,
              servicegroup_id: '1',
            },
          ],
        },
        {
          id: '6',
          name: 'Energisparing',
          comment: null,
          helpText: null,
          grouping: 'info',
          Services: [
            {
              id: '19',
              name: 'Energirådgivning',
              comment: '',
              helpText:
                'En energirådgiver går gjennom hele bygningskroppen og gir deg en rapport med en vurdering av hvilke energifrigjøringstiltak som vil være mest effektive i ditt bygg.',
              active: true,
              servicegroup_id: '6',
            },
            {
              id: '28',
              name: 'Balansert ventilasjon',
              comment: '',
              helpText:
                'Et balansert ventilasjonsanlegg er et viftesystem som erstatter brukt luft med frisk, renset luft. Ventilasjonsanlegget filtrerer uteluften og fjerner fukt. Dette gir et godt inneklima og reduserer faren for fuktskader og kondens.',
              active: true,
              servicegroup_id: '6',
            },
            {
              id: '29',
              name: 'Energioppgradering',
              comment: '',
              helpText:
                'En energioppgradering innebærer at det gjennomføres en helhetlig og systematisk oppgradering av bygningskroppen. ',
              active: true,
              servicegroup_id: '6',
            },
            {
              id: '30',
              name: 'Etterisolering',
              comment: '',
              helpText:
                'Ved å etterisolere tak, vegger, gulv og kjeller reduseres varmetapet fra bygningen og dermed også energikostnadene.  Etterisolering kan gjøres stegvis eller samlet.',
              active: true,
              servicegroup_id: '6',
            },
            {
              id: '31',
              name: 'Lavenergivinduer',
              comment: '',
              helpText:
                'Lavenergivinduer er mer enn dobbelt så energieffektive som gamle vinduer. Detter gir lavere energibruk og bedre komfort. ',
              active: true,
              servicegroup_id: '6',
            },
          ],
        },
        {
          id: '8',
          name: 'Fjernvarme',
          comment: '',
          helpText: '',
          grouping: 'heating',
          Services: [
            {
              id: '22',
              name: 'Fjernvarme',
              comment: '',
              helpText:
                'Et fjernvarmeanlegg er et varmeannlegg som forsyner flere bygg med energi til varmt tappevann og oppvarming.',
              active: true,
              servicegroup_id: '8',
            },
          ],
        },
        {
          id: '2',
          name: 'Bioenergi',
          comment: null,
          helpText: null,
          grouping: 'heating',
          Services: [
            {
              id: '24',
              name: 'Fliskjel',
              comment: '',
              helpText: '',
              active: true,
              servicegroup_id: '2',
            },
          ],
        },
      ],
    },
    providers: {},
    selectedProviders: {},
    select: {
      selectedProvider: {},
      toggledProvider: {},
    },
  }
}

export function getPageState(pageNr) {
  const pages = {
    2: p2,
  }

  if (typeof pages[pageNr] === 'function') {
    return pages[pageNr]()
  }
}
