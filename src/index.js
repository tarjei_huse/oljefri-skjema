import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'

import qs from 'qs'

import reducers, { setApiUrl, setPageAction } from './store'
import SchemaRoot from './SchemaRoot'

import { Provider } from 'react-redux'

import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import smoothscroll from 'smoothscroll-polyfill'

import './index.css'

// kick off the polyfill!
smoothscroll.polyfill()
//import { getPageState } from './testStates'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

let pnr = document.getElementById('postnummer').value
let site =
  window.location.hostname.indexOf('energismart.no') > -1
    ? 'energismart'
    : 'oljefri'
let initialState = {
  select: {
    selectedProvider: {},
    toggledProvider: {},
  },
}
let page

if (process.env.NODE_ENV === 'development') {
  console.log('process.env.NODE_ENV', process.env.NODE_ENV)
  const params = qs.parse(window.location.search.substring(1))
  page = parseInt(params.page, 10)

  /*if (page) {
    initialState = getPageState(page)
    console.log('Using initialState for ', page, initialState)
  }*/

  if (params.pnr) {
    pnr = params.pnr
  }

  if (params.site) {
    site = params.site
  }
}

if (pnr) {
  initialState.fields = {
    postnummer: pnr,
  }
}
const element = document.getElementById('showform')
if (element && element.getAttribute('data-schemaurl')) {
  setApiUrl(element.getAttribute('data-schemaurl'))
}

const store = createStore(
  reducers,
  initialState,
  composeEnhancers(applyMiddleware(thunk))
)

render(
  <Provider store={store}>
    <SchemaRoot site={site} />
  </Provider>,
  element
)

if (page) {
  for (let i = 2; i <= page; i += 1) {
    console.log('setpage:', i)
    store.dispatch(setPageAction(i))
  }
}
