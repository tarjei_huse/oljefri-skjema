import PropTypes from 'prop-types'
import React from 'react'
import ReactTooltip from 'react-tooltip'

Page1.propTypes = {
  lang: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  setPage: PropTypes.func.isRequired,
  setField: PropTypes.func.isRequired,
}

export default function Page1(props) {
  const { lang } = props
  const isValidPostnr =
    props.fields.postnummer &&
    parseInt(props.fields.postnummer, 10) > 0 &&
    parseInt(props.fields.postnummer, 10) < 10000
  function onSubmit(event) {
    event.preventDefault()
    if (isValidPostnr) {
      props.setPage(2)
    }
  }

  return (
    <div id="form_step_1">
      <div className="row">
        <div className="medium-6 columns">
          <h4>{lang.page1_title}</h4>
          <p>
            {lang.page1_tooltip_start}
            <span
              title={lang.page1_tooltip}
              tabIndex={1}
              data-disable-hover="false"
              className="has-tip"
              aria-haspopup="true"
              data-for="kvalitet"
              data-tip>
              {lang.page1_tooltip_title}
            </span>
            {lang.page1_tooltip_end}
          </p>
        </div>
        <div className="medium-6 columns">
          <form onSubmit={onSubmit}>
            <div className="input-group">
              <input
                type="tel"
                name="postnummer"
                placeholder="Ditt postnummer"
                data-field="postnummer"
                value={props.fields.postnummer || ''}
                className="input-group-field postnumber"
                onChange={props.setField}
              />
              <div className="input-group-button">
                <button
                  type="submit"
                  disabled={!isValidPostnr}
                  onClick={props.setPage}
                  className="button"
                  data-page="2">
                  Søk »
                </button>
              </div>
            </div>
          </form>
          {!isValidPostnr &&
            props.fields.postnummer && (
              <div className="form-error is-visible">
                {lang.postnummer_feil}
              </div>
            )}
        </div>
      </div>
      <ReactTooltip
        id={'kvalitet'}
        class="tooltip"
        aria-haspopup="true"
        place="bottom"
        type="info"
        effect="solid">
        <div>
          <p>
            Energispesialist er en kvalitetssikringsordning som gjør det enklere
            og tryggere å finne bedrifter som spesialiserer seg på energitiltak.
            Energispesialist kvalitetssikrer bedrifter innen varme, ventilasjon,
            rådgivning, styring, rehabilitering og sanering.
          </p>
          <p>
            Bedriftene som er tatt opp som energispesialist innfridde krav til
            erfaring, organisering, økonomi, kompetanse og kundeoppfølging, og
            er gjennom en grundig søknadsprosess blitt godkjent
            energispesialist. Les mer om kravene vi stiller til bedriftene
            tilknyttet ordningen i hovedmenypunktet Energispesialist.
          </p>
        </div>
      </ReactTooltip>
    </div>
  )
}
