import React from 'react'

export default function Select(props) {
  const {
    fieldName,
    setField,
    options,
    fields,
    placeholder,
    className,
    error,
  } = props

  return (
    <div className="large-9 small-12 columns">
      <select
        className={className}
        data-field={fieldName}
        onChange={setField}
        value={fields[fieldName] || ''}>
        <option value="" disabled hidden>
          {placeholder}
        </option>
        {Object.keys(options).map(value => {
          const label = options[value]
          return (
            <option key={value} value={value}>
              {label}
            </option>
          )
        })}
      </select>
      {error && (
        <div className="form-error" style={{ display: 'block' }}>
          {error}
        </div>
      )}
    </div>
  )
}
