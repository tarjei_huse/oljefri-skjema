export const serviceGroupings = {
  oljefri: [
    {
      name: 'Fjern oljetanken',
      services: [9],
    },

    {
      name: 'Erstatt oljekjelen',
      services: [2, 3, 14, 15, 22, 23, 24, 26],
    },
    {
      name: 'Erstatt parafinkaminen',
      services: [5, 6, 13],
    },
    {
      name: 'Andre energitiltak',
      contracted: true,
      services: [11, 28, 29, 30, 31, 25, 17, 19],
    },
  ],

  energismart: [
    {
      name: 'Oppvarming',
      services: [2, 3, 5, 6, 13, 14, 15, 17, 22, 23, 24, 25, 26],
    },
    {
      name: 'Rehabilitering',
      services: [29, 30, 31, 9],
    },
    {
      name: 'Andre energitiltak',
      services: [11, 19, 28],
    },
  ],
}

function mapRevgroupings(serviceGrouping) {
  const reverseGrouping = {}
  for (let g of serviceGrouping) {
    for (let gs of g.services) {
      if (!reverseGrouping[gs]) {
        reverseGrouping[gs] = []
      }
      reverseGrouping[gs].push(g.name)
    }
  }
  return reverseGrouping
}

export const reverseGroupings = {
  oljefri: mapRevgroupings(serviceGroupings['oljefri']),
  energismart: mapRevgroupings(serviceGroupings['energismart']),
}
