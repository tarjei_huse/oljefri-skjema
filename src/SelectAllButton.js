import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './SelectAllButton.css'

export default class SelectAllButton extends Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
  }

  onClick = e => {
    this.props.onClick(e)
  }

  onChange() {}

  render() {
    const { isChecked, children } = this.props
    const classNames = ['SelectAllButton']
    if (isChecked) {
      classNames.push('SelectAllButton-selected')
    }
    return (
      <label className={classNames.join(' ')} onClick={this.onClick}>
        <input
          style={{ marginTop: 0, marginBottom: 0 }}
          type="checkbox"
          id="SelectAllButton-input"
          checked={isChecked ? true : false}
          onChange={this.onChange}
        />
        {children}
      </label>
    )
  }
}
