import React from 'react'

export default function Progress(props) {
  const progress = props.progress
  return (
    <div
      aria-valuemax={100}
      aria-valuetext={progress + ' prosent'}
      aria-valuemin={0}
      aria-valuenow={progress}
      tabIndex={0}
      role="progressbar"
      className="progress">
      <div style={{ width: progress + '%' }} className="progress-meter">
        {progress}%
      </div>
    </div>
  )
}
