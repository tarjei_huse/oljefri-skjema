class MiniNode {
  data = {}

  constructor(field, value) {
    const props = field.props()

    Object.keys(props).forEach(key => {
      if (key.indexOf('data-') === 0) {
        this.data[key] = props[key]
      } else if (key === 'value' || key === 'checked' || key === 'type') {
        this[key] = props[key]
      }
    })
    if (props.type === 'checkbox' || props.type === 'radio') {
      this.checked = value
    } else {
      this.value = value
    }
  }

  getAttribute = key => {
    return this.data[key]
  }
}

export default function setValue(page, fieldName, value) {
  //    console.log("setValue", fieldName)
  let field
  if (fieldName === false) {
    field = page
  } else {
    field = page.find(fieldName)
  }
  if (field.length > 1) {
    field = field.filterWhere(f => {
      const type = f.type()
      return ['input', 'select', 'option'].indexOf(type) > -1
    })
  }
  // , '#' + fieldName + ' not found'
  expect(field.length).toBe(1)
  const target = new MiniNode(field, value)
  field.simulate('change', { target })
}
