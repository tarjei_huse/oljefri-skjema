import React from 'react'

import { storiesOf } from '@storybook/react'
//import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import { Welcome } from '@storybook/react/demo'
import { providerQueryResponse } from '../fixtures'
import BottomButtons from '../BottomButtons'
import { SchemaRoot } from '../SchemaRoot'

storiesOf('Welcome', module).add('to Storybook', () => (
  <Welcome showApp={linkTo('Button')} />
))

storiesOf('Page4', module).add('Some providers', () => {
  return (
    <div style={{ maxWidth: '1200px' }}>
      <SchemaRoot
        {...{
          page: 4,
          postnummer: 5010,
          selectedServices: {},
          services: {},
          providers: providerQueryResponse(),
          select: (s, id) => {
            console.log('onSelectProvider:', s, id)
          },
          selectedProviders: { 41: true, 19: false },
          toggledProviders: {},
          fields: {},
          site: 'oljefri',
        }}
      />
    </div>
  )
})
function noop() {
  console.log('noop')
}

storiesOf('BottomButtons', module).add('BottomButtons', () => {
  return (
    <div
      style={{
        maxWidth: '1200px',
        padding: '4rem',
        width: '800px',
        border: '1px solid yellow',
      }}>
      <BottomButtons
        currentStep={3}
        onNextPage={noop}
        nextPageError={'next error'}
        nextPageEnabled={true}
        nextPageLabel="Label for next"
        setPage={noop}
      />
    </div>
  )
})
