export const page4state = {
  error: {},
  fields: {
    postnummer: '0575',
    postPostcode: '0575',
    postPostplace: 'Oslo',
    buildingType_id: '2',
    yearBuilt: '1910',
    area: '90'
  },
  page: 4,
  postnummer: {
    providerCount: 16,
    hasProviders: true,
    poststed: 'Oslo',
    activeKommune: true
  },
  serviceGroups: {
    groups: [
      {
        id: '1',
        name: 'Varmepumpe',
        comment: '',
        helpText: '',
        grouping: 'heating',
        ServiceTypes: [
          {
            id: '1',
            name: 'Varmepumpe',
            comment: '',
            helpText: 'Varmepumper henter energi ut fra et område med lav temperatur, hever temperaturen og avgir varmen til bygget. Med en varmepumpe får man igjen mellom 2 og 4 ganger så mye varme som strøm man tilfører, avhengig av type og temperatur. For boliger med vannbåren varme er luft-vann-varmepumpe (med uteluft som varmekilde) eller væske-vann-varmepumpe (med berg, sjø eller jord som varmekilde) de vanligste løsningene.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '1'
          },
          {
            id: '5',
            name: 'Varmepumpe',
            comment: '',
            helpText: 'En luft/luft-varmepumpe henter energi fra uteluften og sprer varmen i bygget ved hjelp av en vifte.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '1',
            servicegroup_id: '1'
          },
          {
            id: '12',
            name: 'Varmepumpe',
            comment: '',
            helpText: 'En luft/luft-varmepumpe henter energi fra uteluften og sprer varmen i bygget ved hjelp av en vifte.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '1',
            servicegroup_id: '1'
          },
          {
            id: '17',
            name: 'Varmepumpe luft-luft',
            comment: '',
            helpText: 'En luft/luft-varmepumpe henter energi fra uteluften og sprer varmen i bygget ved hjelp av en vifte.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '1'
          },
          {
            id: '18',
            name: 'Varmepumpe luft-vann',
            comment: '',
            helpText: 'En luft/vann-varmepumpe henter energi fra uteluften og avgir varme i bygget via et vannbårent system, radiatorer eller konvektorer. ',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '1'
          },
          {
            id: '19',
            name: 'Varmepumpe væske-vann',
            comment: '',
            helpText: 'En væske/vann-varmepumpe kan hente energi fra grunnvann, sjøvann, berg eller jord og avgi varme i bygget via et vannbårent system, radiatorer eller konvektorer.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '1'
          }
        ]
      },
      {
        id: '5',
        name: 'Solenergi',
        comment: null,
        helpText: null,
        grouping: 'heating',
        ServiceTypes: [
          {
            id: '4',
            name: 'Solenergi',
            comment: '',
            helpText: 'Solfangere på tak eller fasade varmer opp vann som kan brukes til oppvarming av boarealer eller tappevann. Solceller utnytter solenergi til å produsere strøm.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '5'
          },
          {
            id: '24',
            name: 'Solvarme (solfanger)',
            comment: '',
            helpText: 'Solfangere på tak eller fasade varmer opp vann som kan brukes til oppvarming av boarealer eller tappevann. ',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '5'
          },
          {
            id: '8',
            name: 'Solenergi',
            comment: '',
            helpText: 'Solfangere på tak eller fasade varmer opp vann som kan brukes til oppvarming av tappevann. Solceller utnytter solenergi til å produsere strøm.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '1',
            servicegroup_id: '5'
          },
          {
            id: '15',
            name: 'Solstrøm (solceller)',
            comment: '',
            helpText: 'Solcellepanel på tak eller fasade kan omdanne solenergi til elektrisitet. ',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '1',
            servicegroup_id: '5'
          },
          {
            id: '16',
            name: 'Solvarme (til oppvarming av varmtvann)',
            comment: '',
            helpText: 'Solfangere kan brukes til oppvarming av tappevann.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '1',
            servicegroup_id: '5'
          },
          {
            id: '23',
            name: 'Solstrøm (solceller)',
            comment: '',
            helpText: 'Solcellepanel på tak eller fasade kan omdanne solenergi til elektrisitet. ',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '5'
          }
        ]
      },
      {
        id: '6',
        name: 'Energisparing',
        comment: null,
        helpText: null,
        grouping: 'info',
        ServiceTypes: [
          {
            id: '10',
            name: 'Strømstyringssystem',
            comment: '',
            helpText: 'Med elektronisk styring av varmebehov og belysning kan du redusere energiforbruket.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '0',
            servicegroup_id: '6'
          },
          {
            id: '27',
            name: 'Energistyringssystemer',
            comment: '',
            helpText: 'Med elektronisk styring av varmebehov og belysning kan energiforbruket reduseres.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '0',
            servicegroup_id: '6'
          },
          {
            id: '26',
            name: 'Energirådgivning',
            comment: '',
            helpText: 'En energirådgiver går gjennom hele bygningskroppen og gir deg en rapport med en vurdering av hvilke energifrigjøringstiltak som vil være mest effektive i ditt bygg.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '0',
            servicegroup_id: '6'
          },
          {
            id: '9',
            name: 'Termografering og trykktesting',
            comment: '',
            helpText: 'En termografør tar bilder som måler varmetapet i ulike deler av boligen, mens trykktesting viser utettheter og hvor mye uønsket kald luft som slippes inn. Dette er virkemidler som synliggjør akkurat hvilke områder i boligen du bør prioritere å gjøre noe med for å få ned energiforbruket mest mulig.\r\n',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '0',
            servicegroup_id: '6'
          },
          {
            id: '25',
            name: 'Trykktesting og termografering',
            comment: '',
            helpText: 'Med termografering og trykktesting kan du få svar på hvor tett bygget er, og hvor varmen forsvinner ut. ',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '0',
            servicegroup_id: '6'
          },
          {
            id: '31',
            name: 'Energirådgivning',
            comment: '',
            helpText: 'En energirådgiver går gjennom boligen din, og vurderer hvilke tiltak som kan og bør gjennomføres for å redusere energiforbruket mest mulig.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '0',
            servicegroup_id: '6'
          }
        ]
      },
      {
        id: '3',
        name: 'Trepellets',
        comment: null,
        helpText: null,
        grouping: 'heating',
        ServiceTypes: [
          {
            id: '2',
            name: 'Pelletskjel og -brenner',
            comment: '',
            helpText: 'Pellets er komprimert trevirke og kan brennes i kjeler tilknyttet et vannbårent anlegg. Om du har oljefyringsanlegg kan det være nok å skifte ut brenneren. Pellets kan også brennes i pelletskaminer. Noen av disse har vannkappe, og kan inngå i et vannbårent varmeanlegg.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '3'
          }
        ]
      },
      {
        id: '2',
        name: 'Bioenergi',
        comment: null,
        helpText: null,
        grouping: 'heating',
        ServiceTypes: [
          {
            id: '20',
            name: 'Biofyringsanlegg',
            comment: '',
            helpText: 'Bioenergi omfatter blant annet ved, pellets, briketter og flis. Et biofyringsanlegg kan varme opp vann til tappevann og oppvarming av bygget via et vannbårent system.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '2'
          },
          {
            id: '22',
            name: 'Pelletskamin',
            comment: '',
            helpText: 'Moderne pelletskaminer sirkulerer varmen i rommet med en vifte og kan tennes og slukkes automatisk av en romtermostat. Pellets er komprimert trevirke, og gir høy temperatur og lite aske og partikkelutslipp ved forbrenning.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '2'
          },
          {
            id: '21',
            name: 'Rentbrennende vedovn',
            comment: '',
            helpText: 'Moderne vedovner produserer mye varme og har lave partikkelutslipp.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '2'
          }
        ]
      },
      {
        id: '4',
        name: 'Ved',
        comment: null,
        helpText: null,
        grouping: 'heating',
        ServiceTypes: [
          {
            id: '6',
            name: 'Rentbrennende vedovn, peis eller innsats',
            comment: '',
            helpText: 'Moderne vedovner produserer mye varme og har lave partikkelutslipp.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '1',
            servicegroup_id: '4'
          },
          {
            id: '13',
            name: 'Rentbrennende vedovn',
            comment: '',
            helpText: 'Moderne vedovner produserer mye varme og har lave partikkelutslipp.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '1',
            servicegroup_id: '4'
          },
          {
            id: '3',
            name: 'Vedkjel og vedovn med vannkappe',
            comment: '',
            helpText: 'Vedfyring foregår som regel i vanlige vedovner, men det er mulig å bruke vedkjeler som kan varme opp både forbruksvann og boligens arealer. Noen vedovner har vannkappe. Disse kan også levere varme til et vannbårent varmesystem.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '2',
            servicegroup_id: '4'
          }
        ]
      },
      {
        id: '7',
        name: 'Rens og fjerning av oljetank',
        comment: null,
        helpText: null,
        grouping: 'oiltank',
        ServiceTypes: [
          {
            id: '11',
            name: 'Rens og fjerning av oljetank',
            comment: '',
            helpText: 'Tanker som ikke er i bruk må tømmes og graves opp dersom ikke annen særskilt tillatelse er gitt.',
            market: 'private',
            active: true,
            requiresWaterborneHeating: '0',
            servicegroup_id: '7'
          },
          {
            id: '28',
            name: 'Fjerning av oljetank',
            comment: '',
            helpText: 'Tanker som ikke er i bruk må tømmes og graves opp dersom ikke annen særskilt tillatelse er gitt.',
            market: 'business',
            active: true,
            requiresWaterborneHeating: '0',
            servicegroup_id: '7'
          }
        ]
      }
    ]
  },
  providers: [
    {
      id: '60',
      contact_id: '1996',
      description: 'Aktiv Tankrens AS ble etablert i 1986. Vi har fire slamsugere med hengere. Holder til på Risløkka i Oslo men tar jobber over hele landet. Rens av tank, oppgraving eller nedskjæring av tank, demontering og fjerning av fyrkjeler, distribusjon av bioolje. Gjør jobber for både private, borettslag, bedrifter og de store oljeselskapene. ',
      intervalStart: '0',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '841918142',
      logo: 'logos/b2f930d1bfeba311363c5490f7df3de06002cd7b.jpg',
      mainContact: {
        id: '1996',
        firstname: 'Pål',
        lastname: 'Christophersen',
        jobRole: 'Daglig leder',
        mobile: '',
        phone: '23052710',
        email: 'tankrens@online.no',
        description: '',
        postAddress: 'Østre Aker vei 60',
        postPostcode: '581',
        sfguarduser_id: '62',
        newsletterAllowed: false,
        termsAgreed: true,
        type: '1',
        firmname: 'Aktiv Tankrens AS',
        website: 'http://aktivtankrens.no',
        visitingAddress: 'Østre Aker vei 60',
        visitingPostcode: '581'
      },
      Services: [
        {
          id: '11',
          name: 'Rens og fjerning av oljetank'
        }
      ]
    },
    {
      id: '41',
      contact_id: '929',
      description: 'Arbeider i hovedsak med vannbårende oppvarmings systemer. \r\nLang erfaring med varmeanlegg/varmepumpeanlegg / Solvarme/ gulvvarmeanlegg \r\nstore og små oppdrag. \r\nAlt av rørleggertjenester \r\n',
      intervalStart: '0',
      intervalEnd: '2000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '897175622',
      logo: 'logos/a37963313b45165c55d2ee68450f9d52f9b77965.jpg',
      mainContact: {
        id: '929',
        firstname: 'Fredrik',
        lastname: 'Sørnes',
        jobRole: 'Rørleggermester',
        mobile: '90815225',
        phone: '90815225',
        email: 'fredrik@csvarme.no',
        description: '',
        postAddress: 'Vestsideveien 481',
        postPostcode: '3410',
        sfguarduser_id: '43',
        newsletterAllowed: false,
        termsAgreed: true,
        type: '1',
        firmname: 'CS Varme og Energiteknikk AS',
        website: null,
        visitingAddress: 'Vestsideveien 481',
        visitingPostcode: '3410'
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        },
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '5',
          name: 'Varmepumpe'
        },
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '19',
      contact_id: '34',
      description: 'Vi har levert over 500 bergvarmepumper i Oslo.\r\nElektropluss er boligvarmespesialisten. Med både rørleggere og elektrikere i stallen kan vi levere et komplett, gjennomtenkt anlegg uten å involvere andre aktører. Vi leverer alle former for boligvarme og fører flere ledende merker. Når du snakker med oss kan du derfor være trygg på at vi gjør en individuell vurdering av varmekilde og produkt. Ingen skal kunne slå oss på kundetilfredshet. Ta gjerne en titt på anbefalt.no for å se anbefalinger fra noen av våre kunder. Alle våre varmepumper leveres med 5 års garanti\r\n',
      intervalStart: '1',
      intervalEnd: '10000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '880 856 642',
      logo: 'logos/fcdc5ded27d4c82522204d059ab50733b321a546.jpg',
      mainContact: {
        id: '34',
        firstname: 'Halvor',
        lastname: 'Langkaas',
        jobRole: 'Daglig leder',
        mobile: '91757514',
        phone: '',
        email: 'oslo@elektropluss.no',
        description: '',
        postAddress: 'Nils Lauritssønsvei 39',
        postPostcode: '853',
        sfguarduser_id: '20',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Elektropluss AS',
        website: 'http://www.elektropluss.no/oslo-tasen.html',
        visitingAddress: '',
        visitingPostcode: null
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        },
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '5',
          name: 'Varmepumpe'
        },
        {
          id: '10',
          name: 'Strømstyringssystem'
        },
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '77',
      contact_id: '3187',
      description: 'Energibygg AS ble etablert i 2010 og tilbyr tjenester knyttet til energi i bygg. Våre rådgivere har høy kompetanse innen et bredt fagfelt og kan bidra på mange områder i et prosjekt. Vi har kontor i Oslo, Hamar og Horten med store og små oppdragsgivere fra hele landet. Våre primære tjenester er energirådgivning/-kartlegging, tetthetskontroll og termografering. Vi gjør også mer avansert beregning og simulering av energibruk i nye og eksisterende bygg, samt energimerking. Vi kjennetegnes av profesjonalitet, fleksibilitet og løsningsvilje.',
      intervalStart: '0',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '895425192',
      logo: 'logos/9837f68ec1cd29a7630d18138606ecf0009e0549.jpg',
      mainContact: {
        id: '3187',
        firstname: 'Anders ',
        lastname: 'Enger',
        jobRole: 'Daglig leder',
        mobile: null,
        phone: '95826291',
        email: 'okf@energibygg.no',
        description: '',
        postAddress: 'St. Jørgens gate 1A',
        postPostcode: '2315',
        sfguarduser_id: '79',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Energibygg AS',
        website: 'http://www.energibygg.no',
        visitingAddress: 'St. Jørgens gate 1A',
        visitingPostcode: '2315'
      },
      Services: [
        {
          id: '9',
          name: 'Termografering og trykktesting'
        },
        {
          id: '31',
          name: 'Energirådgivning'
        }
      ]
    },
    {
      id: '80',
      contact_id: '3565',
      description: '12 ansatte med bred erfaring innen hele rørleggerfaget, men med spisskompetanse innen energiøkonomisering med varmepumper væske/vann, luft/vann, luft/luft og fjernvarmesentraler. Leverer komplette løsninger til privat og næring samt servicer og feilretting. ',
      intervalStart: '0',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '947429922',
      logo: 'logos/3f2848c743560bed6e373c592bf3242b80ae31c8.jpg',
      mainContact: {
        id: '3565',
        firstname: 'Hans-Erik',
        lastname: 'Fossum',
        jobRole: 'Daglig leder',
        mobile: null,
        phone: '91663333',
        email: 'hans-erik@fossum.as',
        description: '',
        postAddress: 'Postboks 41',
        postPostcode: '623',
        sfguarduser_id: '82',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Fossum & Kristiansen AS',
        website: null,
        visitingAddress: 'Strømsveien 346',
        visitingPostcode: '1081'
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        },
        {
          id: '2',
          name: 'Pelletskjel og -brenner'
        },
        {
          id: '3',
          name: 'Vedkjel og vedovn med vannkappe'
        },
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '5',
          name: 'Varmepumpe'
        },
        {
          id: '8',
          name: 'Solenergi'
        },
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '18',
      contact_id: '33',
      description: 'VVS-firma med lang erfaring med bergvarmepumper. Leverer også luft/vann-anlegg',
      intervalStart: '6',
      intervalEnd: '100000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '980 514 889',
      logo: 'logos/d71232d1f60216d5d3743f765b7136d759d24d71.jpg',
      mainContact: {
        id: '33',
        firstname: 'Ann-Cathrin',
        lastname: 'Gulbrandsen',
        jobRole: 'Kontoransvarlig',
        mobile: '92892710',
        phone: '22143503',
        email: 'ann-cathrin@hvt.no',
        description: '',
        postAddress: 'Huldreveien 11',
        postPostcode: '781',
        sfguarduser_id: '19',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Hertzberg Varmeteknikk AS',
        website: 'http://hvt.no/',
        visitingAddress: '',
        visitingPostcode: null
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        }
      ]
    },
    {
      id: '59',
      contact_id: '1933',
      description: 'Hitech Energy AS er opptatt av å tilby unik løsning skreddersydd etter kundens behov for fornybar energi.  Vi utfører hele prosessen med å ta opp olje-/parafintanken ute, sanerer oljefyr med tilhørende rør, og analyserer den beste løsningen for ny energi.',
      intervalStart: '0',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '886553102',
      logo: 'logos/cf81c32664f4ce9700b97e8e9d2948aecefc3096.jpg',
      mainContact: {
        id: '1933',
        firstname: 'Else',
        lastname: 'Frøholm',
        jobRole: 'HMS/KS-ansvarlig',
        mobile: '90655000',
        phone: '40002862',
        email: 'else.froholm@hitechenergy.no',
        description: '',
        postAddress: 'Postboks 333',
        postPostcode: '1372',
        sfguarduser_id: '61',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Hitech Energy AS',
        website: 'http://www.hitechenergy.no/',
        visitingAddress: 'Solbråtveien 49',
        visitingPostcode: '1383'
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        },
        {
          id: '3',
          name: 'Vedkjel og vedovn med vannkappe'
        },
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '5',
          name: 'Varmepumpe'
        },
        {
          id: '11',
          name: 'Rens og fjerning av oljetank'
        },
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '68',
      contact_id: '2401',
      description: 'Hof Miljøservice AS ble etablert i 1972 og utfører oppdrag i Vestfold, Buskerud, Akershus og Oslo. Vi har lang erfaring innen sanering av oljetanker og har egen gravemaskin og utstyr til å utføre hele jobben selv, slik at våre kunder har kun ett firma å forholde seg til. Ta gjerne kontakt for gratis befaring og uforpliktende tilbud.',
      intervalStart: '0',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '983238858',
      logo: 'logos/5eeb32e924aeff8f89dd30fed1f228a668f559d2.jpg',
      mainContact: {
        id: '2401',
        firstname: 'Thomas',
        lastname: 'Johansen',
        jobRole: 'Daglig leder',
        mobile: null,
        phone: '92881700',
        email: 'hof@hofmiljoservice.no',
        description: '',
        postAddress: 'Industriveien 14',
        postPostcode: '3090',
        sfguarduser_id: '70',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Hof Miljøservice AS',
        website: 'http://www.hofmiljoservice.no/',
        visitingAddress: 'Industriveien 14',
        visitingPostcode: '3090'
      },
      Services: [
        {
          id: '11',
          name: 'Rens og fjerning av oljetank'
        }
      ]
    },
    {
      id: '25',
      contact_id: '309',
      description: 'Vi leverer komplette varmeanlegg og service på varmeanlegg. Fokuserer kun på varme- og energisystemer.',
      intervalStart: '0',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '993 296 627 ',
      logo: 'logos/a7217ad72de79fec43afabcb160787823f9167e9.jpg',
      mainContact: {
        id: '309',
        firstname: 'Christian',
        lastname: 'Jensen',
        jobRole: 'Service tekniker / kalkulasjon',
        mobile: '90614205',
        phone: '66846490',
        email: 'cj@holtefjell.no',
        description: '',
        postAddress: 'Dansrudveien 75\r\n',
        postPostcode: '3036',
        sfguarduser_id: '27',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Holtefjell Energi AS',
        website: 'http://www.holtefjell.no',
        visitingAddress: 'Dansrudveien 75\r\n',
        visitingPostcode: '3036'
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        },
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '5',
          name: 'Varmepumpe'
        },
        {
          id: '8',
          name: 'Solenergi'
        },
        {
          id: '10',
          name: 'Strømstyringssystem'
        },
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '54',
      contact_id: '1522',
      description: 'Ildstedet er navnet på Norges mest erfarne gruppe faghandelsbutikker innen varmebransjen. Vi er eksperter på ovner og peiser, og vi har mye erfaring fra både enkle og kompliserte installasjoner.\r\nI våre butikker finner du store utstillinger av produkter fra Jøtul, Scan, Atra og Ild, i tillegg til en rekke tilbehør og kompletterende produkter. Vi setter vår stolthet i å føre de beste merkevarene, som fokuserer på de samme viktige verdiene som Ildstedet står for: Service, erfaring, sikkerhet og kvalitet.',
      intervalStart: '0',
      intervalEnd: '2000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '',
      logo: 'logos/b5339abc92b818473d3747fc59030490f9d8a49f.jpg',
      mainContact: {
        id: '1522',
        firstname: 'Hilde Jeanette',
        lastname: 'Ødegaard',
        jobRole: 'Markedskoordinator',
        mobile: '913 55 204',
        phone: '69 35 90 61',
        email: 'hilde.odegaard@jotul.no',
        description: '',
        postAddress: '',
        postPostcode: null,
        sfguarduser_id: '56',
        newsletterAllowed: false,
        termsAgreed: true,
        type: '1',
        firmname: 'Ildstedet',
        website: 'http://ildstedet.no/',
        visitingAddress: '',
        visitingPostcode: null
      },
      Services: [
        {
          id: '6',
          name: 'Rentbrennende vedovn, peis eller innsats'
        }
      ]
    },
    {
      id: '22',
      contact_id: '37',
      description: 'Ivar Lærum AS har siden 1949 vært en ledene totalleverandør av varmeanlegg for vannbåren varme. Fra og med varmekilden via distrubisjonsnettet til varmeavgiveren (radiator eller gulvvarmeanlegg.) Vi har lang erfaring og allsidig kunnskap og kan hjelpe deg til å ta  gode og riktige valg i forbindelse med renovering eller nyinstallasjon av vannbåren varme. \r\nBlant våre ansatte finnes VVS-ingeniører, EO - godkjente serviceteknikkere, rørleggere, herav 2 rørleggermestere, elektrikere, feiere og mekanikere. \r\nVi er rustet til å løse alle oppgaver i forbindelse med rehabilitering, installasjon, drift og vedlikehold av varmeanlegg.',
      intervalStart: '5',
      intervalEnd: '1000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '814 080 382 ',
      logo: 'logos/5b090e66a5ab13a0a8efd86e821160f6f2cd35f0.jpg',
      mainContact: {
        id: '37',
        firstname: 'Jan',
        lastname: 'Edvardsen',
        jobRole: 'fagleder',
        mobile: '95243740',
        phone: '22652165',
        email: 'jan@ivarlaerum.no',
        description: '',
        postAddress: 'Haslevangen 45B',
        postPostcode: '579',
        sfguarduser_id: '23',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Ivar Lærum AS',
        website: 'http:/www.ivarlaerum.no/',
        visitingAddress: '',
        visitingPostcode: null
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        },
        {
          id: '2',
          name: 'Pelletskjel og -brenner'
        },
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '8',
          name: 'Solenergi'
        },
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '47',
      contact_id: '1199',
      description: 'Velkommen til din varmepumpe spesialist. Vi tilbyr varmepumpesystemer for bergvarme og luft-vann.Vi leverer varmepumper med 10 års skriftlig garanti. Flere referanser fra tidligere kunder. Ta kontakt for en befaring.\r\n47604400',
      intervalStart: '0',
      intervalEnd: '2000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '888847642',
      logo: 'logos/cf81df334f4e339e194db837b9bf65e455c379eb.jpg',
      mainContact: {
        id: '1199',
        firstname: 'Petter ',
        lastname: 'Olstad',
        jobRole: 'Daglig leder',
        mobile: '47604400',
        phone: '',
        email: 'post@metror.no',
        description: '',
        postAddress: 'Ballerud Alle 1',
        postPostcode: '1363',
        sfguarduser_id: '49',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Metro Rørleggerservice AS',
        website: 'http://metror.no',
        visitingAddress: 'Ballerud Allé 1',
        visitingPostcode: '1363'
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        },
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '74',
      contact_id: '3064',
      description: 'Miljøvakta AS utfører forbyggende tjenester som tankkontroll, tankrengjøring, tankrekondisjonering og tanksanering av alle typer tanker. Miljøvakta AS operer også siden www.tankkontroll.no som gi informasjon om dagens regelverk, samt prisestimater for de ulike typer tankoppdrag. Miljøvakta AS utfører alle tjenestene som rengjøring og avgassing av oljtanker, oppgraving eller nedskjæring av utvendige eller innvendige oljetanker. Klipping eller demontering av fyrkjeler. Miljøvakta AS sender også inn saneringsjournaler til stedlige forvanltningsmyndigheter. Miljøvakta AS utfører gratis befaringer. ',
      intervalStart: '0',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '896546902',
      logo: 'logos/ba453b0d218af81996df881c01866ff2c66191ca.jpg',
      mainContact: {
        id: '3064',
        firstname: 'Fred',
        lastname: 'Ytterdahl',
        jobRole: 'Daglig leder',
        mobile: null,
        phone: '93210567',
        email: 'post@miljovakta.no',
        description: '',
        postAddress: 'Korallsoppveien 2C',
        postPostcode: '1476',
        sfguarduser_id: '76',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Miljøvakta AS',
        website: 'http://miljovakta.no/',
        visitingAddress: 'Korallsoppveien 2C',
        visitingPostcode: '1476'
      },
      Services: [
        {
          id: '11',
          name: 'Rens og fjerning av oljetank'
        }
      ]
    },
    {
      id: '23',
      contact_id: '38',
      description: 'Nordisk Energikontroll AS ble etablert i 1996. Vi har spesialisert oss på styring og overvåking til fyringsanlegg med vannbåren varme. Vi bidrar med produkter og løsninger som gir lavere energikostnader, bedre kontroll på driften og markedstilpasning for å oppnå lavest mulig energikostnader. ',
      intervalStart: '250000',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '976 724 321',
      logo: 'logos/ee0c7c722f9f096cc7a6ac5eaa4e59c317c21e83.jpg',
      mainContact: {
        id: '38',
        firstname: 'Tor',
        lastname: 'Sveine',
        jobRole: 'Daglig leder / VVS-ingeniør',
        mobile: '913 09 703',
        phone: '64 84 55 20',
        email: 'salg@noen.no',
        description: '',
        postAddress: 'Postboks 93',
        postPostcode: '2027',
        sfguarduser_id: '24',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Nordisk Energikontroll AS',
        website: 'http://www.noen.no',
        visitingAddress: 'Hvamstubben 17',
        visitingPostcode: '2013'
      },
      Services: [
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '48',
      contact_id: '1245',
      description: 'Snersrud & Pedersen as har lang erfaring med både nye og eldre varmeanlegg. \r\nVår energirådgiver hjelper deg å finne den beste løsningen for din bolig, og en av våre prosjektledere hjelper deg gjennom hele prosessen. Ta kontakt for en uforpliktende prat.',
      intervalStart: '0',
      intervalEnd: '2000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '984793731',
      logo: 'logos/b40104f9aab33bad2c20c02c34cd6cf9b9736baf.jpg',
      mainContact: {
        id: '1245',
        firstname: 'Ivar',
        lastname: 'Snersrud',
        jobRole: 'Daglig leder',
        mobile: '93066611',
        phone: '',
        email: 'post@sogp.no',
        description: '',
        postAddress: 'Nye Vakåsvei 78',
        postPostcode: '1395',
        sfguarduser_id: '50',
        newsletterAllowed: true,
        termsAgreed: true,
        type: '1',
        firmname: 'Snersrud & Pedersen AS',
        website: null,
        visitingAddress: 'Nye Vakåsvei 78',
        visitingPostcode: '1395'
      },
      Services: [
        {
          id: '1',
          name: 'Varmepumpe'
        },
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '5',
          name: 'Varmepumpe'
        },
        {
          id: '8',
          name: 'Solenergi'
        },
        {
          id: '19',
          name: 'Varmepumpe væske-vann'
        }
      ]
    },
    {
      id: '52',
      contact_id: '1456',
      description: '•\tSolpluss AS er spesialister på solenergi i Norge.\r\n•\tVåre kunder skryter av sine solenergianlegg.\r\n•\tTenk miljø, økonomi og kvalitet når du velger energiløsning.\r\n•\tKontakt oss gjerne for en hyggelig prat om solcelleanlegg og/eller solfangeranlegg.\r\n',
      intervalStart: '0',
      intervalEnd: '5000000',
      status: 'approved',
      comment: '',
      notifyOnRequest: true,
      providerQuality: null,
      orgNr: '993405299',
      logo: 'logos/f69d75315b95e96a667daa7314f78f7ca81c8915.jpg',
      mainContact: {
        id: '1456',
        firstname: 'Yngvar',
        lastname: 'Søetorp',
        jobRole: 'Daglig leder',
        mobile: '48285523',
        phone: '',
        email: 'post@solpluss.no',
        description: '',
        postAddress: 'Solberglien 117',
        postPostcode: '683',
        sfguarduser_id: '54',
        newsletterAllowed: false,
        termsAgreed: false,
        type: '1',
        firmname: 'Solpluss AS',
        website: 'http://www.solpluss.no',
        visitingAddress: 'Solberglien 117',
        visitingPostcode: '683'
      },
      Services: [
        {
          id: '4',
          name: 'Solenergi'
        },
        {
          id: '8',
          name: 'Solenergi'
        }
      ]
    }
  ],
  selectedProviders: {
    '19': true,
    '22': true,
    '23': true,
    '25': true,
    '26': true,
    '41': true,
    '47': true,
    '48': true,
    '52': true,
    '54': true,
    '59': true,
    '60': true,
    '68': true,
    '74': true,
    '77': true,
    '80': true
  },
  select: {
    selectedProvider: {},
    toggledProvider: {},
    serviceGroup: {
      '1': true,
      '2': true,
      '3': true,
      '4': true,
      '5': true,
      '6': true,
      '8': true,
      '9': true,
      '10': true,
      '11': true,
      '12': true,
      '13': true,
      '15': true,
      '16': true,
      '17': true,
      '18': true,
      '19': true,
      '20': true,
      '21': true,
      '22': true,
      '23': true,
      '24': true,
      '25': true,
      '26': true,
      '27': true,
      '28': true,
      '31': true
    }
  },
  requestResult: {}
}