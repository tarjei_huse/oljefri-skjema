import PropTypes from 'prop-types'
import React, { Component } from 'react'
import scrollTop from './scrollTop'
import Progress from './Progress'
import Reveal from './Reveal'
import BottomButtons from './BottomButtons'
import SelectAllButton from './SelectAllButton'
import { serviceGroupings, reverseGroupings } from './serviceGroupings'

import './Page2.css'

function Service(props) {
  const { serviceType, onSelectService, serviceTypeIsChecked } = props

  const classNames = ['Page2-service-type']

  return (
    <div className={classNames.join(' ')}>
      <label htmlFor={serviceType.id} className="Page2-service-type-label">
        <input
          type="checkbox"
          id={serviceType.id}
          className="service-type-checkbox"
          checked={serviceTypeIsChecked || false}
          data-service={serviceType.id}
          onChange={onSelectService}
        />
        {serviceType.name}
        {serviceType.helpText && (
          <Reveal>
            {(open, onOpen, onClose) => {
              return (
                <span>
                  <button
                    onClick={open ? onClose : onOpen}
                    title={serviceType.helpText}
                    data-disable-hover="false"
                    className="Page2-has-tip"
                    aria-haspopup="true">
                    ?
                  </button>
                  {open && (
                    <div className="Page2-service-tip">
                      {serviceType.helpText}
                      <button
                        className="Page2-service-tip-close"
                        type="button"
                        onClick={onClose}>
                        Lukk
                      </button>
                    </div>
                  )}
                </span>
              )
            }}
          </Reveal>
        )}
      </label>
    </div>
  )
}

function getNumSelected(selectedServices) {
  return Object.values(selectedServices).filter(v => v).length
}

export default class Page2 extends Component {
  static propTypes = {
    site: PropTypes.oneOf(['energismart', 'oljefri']).isRequired,
    services: PropTypes.array,
    onSelectService: PropTypes.func.isRequired,
    postnummer: PropTypes.shape({
      hasProviders: PropTypes.bool,
      providerCount: PropTypes.number,
    }).isRequired,
    selectedServices: PropTypes.object.isRequired,
    progress: PropTypes.number.isRequired,
    setPage: PropTypes.func.isRequired,
    lang: PropTypes.object.isRequired,
  }
  state = { showMessages: false }

  onSelectAllServices = event => {
    event && event.preventDefault()
    event && event.stopPropagation()
    const numSelected = getNumSelected(this.props.selectedServices)

    const ids =
      numSelected === this.props.services.length
        ? []
        : this.props.services.map(s => s.id)

    this.props.onSelectMany('service', ids, true)
  }
  onSubmit = event => {
    event.preventDefault()
    this.onClickNext(3)
  }

  onClickNext = event => {
    const { selectedServices } = this.props
    if (getNumSelected(selectedServices) > 0) {
      this.props.setPage(event)
    } else {
      this.setState({ showMessages: true })
    }
  }
  render() {
    const {
      lang,
      selectedServices,
      services,
      onSelectService,
      site,
    } = this.props
    const numSelected = getNumSelected(selectedServices)
    const mustDoSelection = this.state.showMessages && numSelected === 0
    const headings = {}
    const serviceGrouping = serviceGroupings[site]
    const reverseServiceGroupings = reverseGroupings[site]

    if (!serviceGrouping) {
      console.error('Missing serviceGrouping:", ', site)
    }
    for (let i = 0; i < serviceGrouping.length; i++) {
      const gr = serviceGrouping[i]
      headings[gr.name] = {
        name: gr.name,
        serviceTypes: [],
      }
    }

    for (let i = 0; i < services.length; i++) {
      const service = services[i]
      if (!reverseServiceGroupings[service.id]) {
        console.error('Grouping not defined for service:', service)
        continue
      }
      for (let sg of reverseServiceGroupings[service.id]) {
        headings[sg].serviceTypes.push(service)
      }
    }

    return (
      <div id="form_step_2" ref={scrollTop}>
        <h4 className="Page2-headline">
          {lang.page2_select_service}:{' '}
          {/*<span style={{ color: 'red' }}>*</span>*/}
        </h4>
        <form onSubmit={this.onSubmit}>
          <div className="Page2-top">
            <h5 className="Page2-heading-providers">
              {lang.page2_heading.replace(
                'XX',
                this.props.postnummer.providerCount
              )}
            </h5>
            {services &&
              services.length && (
                <SelectAllButton
                  onClick={this.onSelectAllServices}
                  isChecked={numSelected === services.length}>
                  Velg alle energitiltak
                </SelectAllButton>
              )}

            {false && (
              <button
                className={
                  'Page2-button-next Page2-button-next-top' +
                  (services &&
                  services.length &&
                  numSelected === services.length
                    ? ' Page2-button-next-top-visible'
                    : '')
                }
                onClick={this.onClickNext}
                data-page="3"
                type="button">
                {lang.page2_next} »
              </button>
            )}
          </div>
          {mustDoSelection && (
            <div className="Page2-must-select">{lang.page2_error}</div>
          )}
          {serviceGrouping.map(sg => {
            const grouping = headings[sg.name]
            if (grouping.serviceTypes.length === 0) {
              return null
            }

            return (
              <div className="Page2-ServicesGroup" key={sg.name}>
                <h3>{grouping.name}</h3>
                {grouping.serviceTypes.map(service => {
                  return (
                    <Service
                      key={service.id}
                      serviceType={service}
                      serviceTypeIsChecked={selectedServices[service.id]}
                      onSelectService={onSelectService}
                    />
                  )
                })}
              </div>
            )
          })}
          <BottomButtons
            currentStep={2}
            onNextPage={this.onClickNext}
            nextPageError={(mustDoSelection && lang.page2_error) || ''}
            nextPageEnabled={true}
            nextPageLabel={lang.page2_next}
            setPage={this.props.setPage}
          />

          <Progress progress={this.props.progress} />
        </form>
      </div>
    )
  }
}

/*
{"id":"5","name":"Solenergi","comment":null,"helpText":null,"grouping":"heating","Services":[{"id":"8","name":"Solenergi","comment":"","helpText":"Solfangere på tak eller fasade varmer opp vann som kan brukes til oppvarming av tappevann. Solceller utnytter solenergi til å produsere strøm.","market":"private","active":true,"requiresWaterborneHeating":"1","servicegroup_id":"5"}]}
*/

export function Page2NoProviders(props) {
  return (
    <div className="article-content">
      <div className="header">
        <h1>
          <span>Takk for at du vil finne en energispesialist.</span>
        </h1>
      </div>
      <h2 className="digest">
        Takk for ditt ønske om å bli oljefri og energieffektiv!
      </h2>
      <p>
        Vi jobber kontinuerlig med å godkjenne energispesialister, men har
        dessverre ingen i ditt område enda.
      </p>
      <p>
        Naturvernforbundet jobber fortløpende med å utvide tjenesten. Oppfordre
        derfor gjerne din kommune til å gjøre en klimainnsats ved å støtte
        oljefri.no. Da får vi midler til å kartlegge markedet i din kommune og
        knytte avtaler med lokale energispesialister.
      </p>
      <p>
        I mellomtiden håper vi at du kan bruke oljefri.no til å gjøre deg kjent
        med ulike oppvarmingsløsninger og måter du kan bli klimaeffektiv på.
      </p>
      <p>
        <strong>Vil du ha tips når vi får tilbydere i ditt område?</strong>
      </p>
      <p>
        Fyll ut skjemaet under, så kontakter vi deg når tjenesten blir
        tilgjengelig i ditt område.
      </p>
    </div>
  )
}
