import React, { Component } from 'react' // eslint-disable-line

export default class Reveal extends Component {
  state = { open: false }

  onOpen = event => {
    event && event.preventDefault()

    this.setState({ open: true })
  }

  onClose = event => {
    event && event.preventDefault()

    this.setState({ open: false })
  }

  render() {
    return this.props.children(this.state.open, this.onOpen, this.onClose)
  }
}
