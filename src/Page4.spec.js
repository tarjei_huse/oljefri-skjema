import React from 'react'
import 'whatwg-fetch'
import { mount } from 'enzyme'

import { buildingTypesFixture, providerQueryResponse } from './fixtures'

// import lang from './lang'
import sinon from 'sinon'

import Page4 from './Page4'

describe('Page4', function () {
  let fields
  let page
  let toggledProviders

  function makeProps() {
    fields = {}
    toggledProviders = {
      '41': true,
    }
    return {
      site: 'energismart',
      fields: fields,
      setField: (event) => {
        const f = event.target.getAttribute('data-field')
        const v = event.target.value
        fields[f] = v
        page.setProps({ fields })
      },
      buildingTypes: buildingTypesFixture(),
      providers: providerQueryResponse(),
      selectedProviders: {},
      toggledProviders: toggledProviders,
      onToggleProvider: sinon.spy(),
      onSelectProvider: sinon.spy(),
      onSelectMany: sinon.spy(),
      setPage: sinon.spy(),
    }
  }

  function setup(props) {
    props = props || makeProps()
    return mount(<Page4 {...props} />)
  }

  beforeEach(function () {
    page = setup()
  })
  afterEach(function () {
    page = fields = toggledProviders = null
  })

  it('should expand logos', function () {

    page.find("Provider").forEach(p => {
      if (p.props().provider.logo) {
        const path = p.find('.Provider-logo img').props().src
        expect(path).toContain('https://')
        expect(path.split('https').length).toBe(2)


      }
    })

  });

  it('should select all firms on toggle', function () {
    page.find('.SelectAllButton').simulate('click')

    expect(page.props().onSelectMany.called).toEqual(true)
    expect(page.props().onSelectMany.args[0]).toEqual([
      'selectedProvider',
      ['41', '19', '18', '25', '22', '45', '47', '42', '48', '52'],
      true,
    ])
  })

  /*  it('should expand firms on toggle', function() {
    expect(page.find('.provider-41').find('.callout').length).toBe(1)

    expect(page.find('.provider-19').find('.callout').length).toBe(0)

    page
      .find('.provider-19')
      .find('.expand')
      .simulate('click')

    expect(page.props().onToggleProvider.called).toBe(true)

    expect(
      page.props().onToggleProvider.args[0][0].target.getAttribute('data-value')
    ).toEqual('19')
  })
  */
})
