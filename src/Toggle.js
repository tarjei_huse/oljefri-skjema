import PropTypes from "prop-types";
import { Component } from "react";

export default class Toggle extends Component {
  static propTypes = {
    children: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = { toggled: props.toggled || false };
    this.toggle = this.toggle.bind(this);
  }

  render() {
    const children = this.props.children;
    const toggled = this.state.toggled;

    return children(toggled, this.toggle);
  }

  toggle(event) {
    event && event.preventDefault();
    this.setState({ toggled: !this.state.toggled });
  }
}
