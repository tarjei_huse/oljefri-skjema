const lang = {
  postnummer_feil: 'Du må fylle inn et gyldig postnummer',
  header_top: 'Finn energispesialister',
  page1_title:
    'Vil du spare strøm, installere fornybar oppvarming eller fjerne oljetanken?',
  page1_tooltip:
    'Vi har godkjente bedrifter på bakgrunn av økonomi, referanser og formell kompetanse for å gjøre det tryggere for deg.',
  page1_tooltip_title: 'kvalitetssikrede',
  page1_tooltip_start: 'Ja, jeg vil finne ',
  page1_tooltip_end: ' bedrifter som kan hjelpe meg!',

  page2_heading: 'XX energispesialister i ditt område',
  page2_select_service: 'Velg energitiltak',
  page2_next: 'Legg til informasjon om bygningen',
  page2_error: 'Du må velge minst en av tjenestene :)',

  page3_heading: 'Informasjon om bygningen:',
  page3_sub_heading:
    'Vi har XX Energispesialister som kan levere tjenestene du ønsker!',
  page3_subtext: '',
  page3_buildingType_placeholder: 'Velg byggtype',
  page3_buildingType_error: 'Du må velge byggningstype',
  page3_yearBuilt_placeholder: 'Velg byggeår',
  page3_yearBuilt_error: 'Du må velge byggeår (ca)',
  page3_area_placeholder: 'Velg kvadratmeter',
  page3_area_error: 'Du må velge areal',
  page3_area_title: 'Areal (m2):',
  page3_year_title: 'Bygget år:',
  page3_buildingType_title: 'Type bygg:',
  page3_next: 'Velg bedrifter',

  page4_none_found: 'Vi fant ingen tilbydere',

  page5_error_firstname: 'Du må oppgi fornavn',
  page5_error_lastname: 'Du må oppgi etternavn',
  page5_error_postAddress: 'Du må oppgi adresse',
  page5_error_postPostcode: 'Du må fylle ut denne verdien.',
  page5_error_phone: 'Du må oppgi et telefonnummer',
  page5_error_email: 'Du må oppgi en epostadresse',
  page5_error_termsAgreed: 'Du må godta vilkårene',
}
export default lang
