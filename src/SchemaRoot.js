import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import 'whatwg-fetch'

import Page1 from './Page1'
import Page2, { Page2NoProviders } from './Page2'
import Page3 from './Page3'
import Page4 from './Page4'
import Page5 from './Page5'
import Page6 from './Page6'
import ReactTooltip from 'react-tooltip'
import lang from './lang'

import {
  SERVICES,
  SELECTED_PROVIDER,
  TOGGLED_PROVIDER,
  setPageAction,
  setField,
  select,
  selectMany,
} from './store'

// http: //local.oljefri/buildingType/listTypes

function mapStateToProps(state) {
  //  console.log("map to props:", state)
  return {
    page: state.page,
    postnummer: state.postnummer,
    selectedServices: state.select.service || {},
    services: state.serviceGroups.groups || [],
    providers: state.providers,
    selectedProviders: state.select.selectedProvider,
    toggledProviders: state.select.toggledProvider,
    fields: state.fields,
    requestResult: state.requestResult,
  }
}

export class SchemaRoot extends Component {
  static propTypes = {
    site: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props)

    this.setField = this.setField.bind(this)
    this.setPage = this.setPage.bind(this)
    this.handleSelectService = this.handleSelectService.bind(this)
    this.handleSelectProvider = this.handleSelectProvider.bind(this)
    this.handleToggleProvider = this.handleToggleProvider.bind(this)
  }

  render() {
    //console.log("props", this.props)
    return (
      <div className="qualityassuredform active">
        <h3>
          <span>{lang.header_top}</span>
        </h3>
        {this.renderPage()}
        <ReactTooltip />
      </div>
    )
  }

  renderPage() {
    let props = { ...this.props }
    props.setField = this.setField
    props.setPage = this.setPage
    props.lang = lang
    props.progress = [0, 25, 50, 75, 85, 95][this.props.page] || 0

    switch (this.props.page) {
      case 1:
        return <Page1 {...props} />
      case 2:
        if (!props.postnummer.hasProviders) {
          return <Page2NoProviders {...props} />
        }
        props.onSelectService = this.handleSelectService
        return <Page2 {...props} onSelectMany={this.props.selectMany} />
      case 3:
        return <Page3 {...props} />
      case 4:
        return (
          <Page4
            {...props}
            onToggleProvider={this.handleToggleProvider}
            onSelectProvider={this.handleSelectProvider}
            onSelectMany={this.props.selectMany}
          />
        )
      case 5:
        return <Page5 {...props} onSendRequest={this.handleSendRequest} />
      case 6:
        return <Page6 {...props} />
      default:
        console.error('Unsupported page nr:', this.props.page)
        return <Page1 {...props} />
    }
  }

  handleSelectService(event) {
    const serviceTypeId = event.target.getAttribute('data-service')
    // console.log('handleSelectService', serviceTypeId, event.target.value, event.target.checked)
    this.props.select(SERVICES, parseInt(serviceTypeId, 10))
  }

  handleSelectProvider(event) {
    const providerId = event.target.value
    this.props.select(SELECTED_PROVIDER, providerId)
  }

  handleToggleProvider(event) {
    event.stopPropagation()
    const providerId = event.target.getAttribute('data-value')
    this.props.select(TOGGLED_PROVIDER, providerId)
  }

  handleSortProviders(event) {
    let providerSortType = event.target.value
    this.setState(providerSortType)
  }

  handleSendRequest(event) {
    return this.props.sendRequest()
  }

  setField(event) {
    const field = event.target.getAttribute('data-field')
    let value = event.target.value
    if (event.target.type && event.target.type.toLowerCase() === 'checkbox') {
      value = event.target.checked
    }
    this.props.setField(field, value)
  }

  setPage(event) {
    let page
    if (event.target) {
      page = event.target.getAttribute('data-page')
      event.preventDefault()
      event.stopPropagation()
    } else {
      page = event
    }
    this.props.setPage(page)

    if (window.gtag && typeof window.gtag === 'function') {
      window.gtag('event', 'Skjema.sideskifte', {
        event_category: 'pageChange',
        event_label: window.location.host,
        value: page,
      })
    }

    if (window.fbq && typeof window.fbq === 'function') {
      window.fbq('track', 'Energismart_skjema', {
        content_name: 'Page change',
        content_category: 'Energismart_skjema',
        content_ids: [page],
      })
    }
  }
}

const ConnectedRoot = connect(
  mapStateToProps,
  {
    setPage: setPageAction,
    setField,
    selectMany,
    select,
  }
)(SchemaRoot)

export default ConnectedRoot
