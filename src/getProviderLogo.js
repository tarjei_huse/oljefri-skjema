const getProviderLogo = (provider) => {
  let { logo } = provider
  if (logo && logo.indexOf('https://') !== 0 && logo.indexOf('http://') !== 0) {
    logo = 'https://admin.oljefri.no/uploads/' + provider.logo
  }
  return logo
}

export default getProviderLogo
