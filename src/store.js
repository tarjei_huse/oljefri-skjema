import invariant from 'invariant'
import { combineReducers } from 'redux'
import { handleActions, createAction } from 'redux-actions'

import Api from './api'

import buildingTypes from './buildingTypes'

const SET_PAGE = 'SET_PAGE'
const SET_POSTNUMMER = 'SET_POSTNUMMER'
const SET_POSTNUMMER_STATUS = 'SET_POSTNUMMER_STATUS'
const SET_SERVICES = 'SET_SERVICES'
const SET_PROVIDERS = 'SET_PROVIDERS'
const ERROR = 'ERROR'
const SET_FIELD = 'SET_FIELD'
const SELECT = 'SELECT'
const SELECT_MANY = 'SELECT_MANY'
const SET_REQUEST_RESULT = 'SET_REQUEST_RESULT'

export const SERVICES = 'service'
export const SELECTED_PROVIDER = 'selectedProvider'
export const TOGGLED_PROVIDER = 'toggledProvider'

const setPostnummerStatus = createAction(SET_POSTNUMMER_STATUS)
const setServiceGroups = createAction(SET_SERVICES)
const setProviders = createAction(SET_PROVIDERS)
const setRequestResult = createAction(SET_REQUEST_RESULT)
const emitError = createAction(ERROR)

export const setPage = createAction(SET_PAGE)
export const select = createAction(SELECT, (key, id) => {
  return { key, id }
})

export const selectMany = createAction(SELECT_MANY, (key, ids, value) => {
  return { key, ids, value }
})

export const setPostnummer = createAction(SET_POSTNUMMER)
export const setField = createAction(SET_FIELD, (fieldName, value) => {
  return { fieldName, value }
})

const api = new Api()

export function setApiUrl(url) {
  api.setUrl(url)
}

const pageReducer = handleActions(
  {
    SET_PAGE: (state, action) => parseInt(action.payload, 10),
  },
  1
)

const postnummerReducer = handleActions(
  {
    SET_POSTNUMMER: (state, action) => {
      let s = { ...state }
      s.postnummer = action.payload
      return s
    },
    SET_POSTNUMMER_STATUS: (state, action) => {
      let s = {
        ...state,
        providerCount: parseInt(action.payload.providerCount, 10),
        hasProviders: action.payload.hasProviders,
        poststed: action.payload.poststed,
        activeKommune: action.payload.activeKommune,
      }
      return s
    },
  },
  {}
)

const serviceGroupsReducer = handleActions(
  {
    SET_SERVICES: (state, action) => ({
      groups: action.payload,
    }),
  },
  {}
)

const selectReducer = handleActions(
  {
    SELECT: (state, action) => {
      let nState = Object.assign({}, state)
      const { key, id } = action.payload
      nState[key] = Object.assign({}, nState[key] || {})
      nState[key][id] = !nState[key][id]
      return nState
    },
    SELECT_MANY: (state, action) => {
      let nState = Object.assign({}, state)
      const { key, ids, value } = action.payload
      //nState[key] = Object.assign({}, nState[key] || {})
      let keysMap = {}
      for (let id of ids) {
        keysMap[id] = value
      }
      nState[key] = keysMap
      return nState
    },
  },
  {}
)

const providersReducer = handleActions(
  {
    SET_PROVIDERS: (state, action) => action.payload,
  },
  {}
)

const requestResultReducer = handleActions(
  {
    SET_REQUEST_RESULT: (state, action) => action.payload,
  },
  {}
)

const errorReducer = handleActions(
  {
    ERROR: (state, action) => ({
      message: action.payload.message,
    }),
  },
  {}
)

const fieldsReducer = handleActions(
  {
    SET_FIELD: (state, action) => {
      state = { ...state }
      state[action.payload.fieldName] = action.payload.value
      return state
    },
  },
  {}
)

const selectedProvidersReducer = handleActions(
  {
    SET_PROVIDERS: (state, action) => {
      if (Object.keys(state).length === 0) {
        let newState = {}
        action.payload.forEach(p => {
          newState['' + p.id] = true
        })
        return newState
      }
      return state
    },
  },
  []
)

export function getPostnummerDataAction() {
  return function(dispatch, getState) {
    const postnummer = getState().fields.postnummer

    return api
      .fetchPostnummerData(postnummer)
      .then(response => {
        // console.log('Got response:', response)
        dispatch(setPostnummerStatus(response))
        return api
          .fetchServices({
            postnummer: postnummer,
            market: 'both',
            wantsOilTankOffers: '1',
          })
          .then(_response => {
            dispatch(setServiceGroups(_response))
          })
      })
      .catch(e => {
        console.error('getPostnummerData error:', e, e.message, e.stack)
        dispatch(emitError('Klarte ikke å slå opp postnummer!'))
      })
  }
}

function getSelected(obj) {
  let ret = []
  Object.keys(obj).forEach(key => {
    if (obj[key] === true) {
      ret.push(key)
    }
  })
  return ret
}

function getAttr(fieldName, fields, _default) {
  return fields.hasOwnProperty(fieldName) ? fields[fieldName] : _default
}

export function getProviders() {
  return function(dispatch, getState) {
    const { fields } = getState()

    const buildingType = getAttr('buildingType_id', fields, null)
    let market = null

    if (buildingType) {
      let type = buildingTypes.filter(bt => bt.id === buildingType).pop()

      if (!type) {
        console.error('Invalid buildingType: ', buildingType)
      } else {
        if (type.private && type.business) {
          market = 'both'
        } else if (type.private || type.business) {
          market = type.private ? 'private' : 'business'
        }
      }
    }

    let query = {
      format: 'json',
      buildingType,
      market,

      postnummer: fields.postnummer,
      area: getAttr('area', fields, null),
      serviceTypes: [],
      /* energyUsage[totalOilUsage]: ,
      waterBorneHeating: 0,
      serviceTypes[]: 6,
      serviceTypes[]: 11,
      market: private,
      wantsOilTankOffers: 1,
      format: jsonarea: 54,
      buildingType: 2,
      postnummer[]: 0575,
      energyUsage[totalOilUsage]: ,
      waterBorneHeating: 0,
      serviceTypes[]: 6,
      serviceTypes[]: 11,

      wantsOilTankOffers: 1,
      */
    }

    query.serviceTypes = getSelected(getState().select[SERVICES] || {})

    Object.keys(query).forEach(key => {
      if (query[key] === null || typeof query[key] === 'undefined') {
        delete query[key]
      }
    })

    return api.fetchProviders(query).then(response => {
      dispatch(setProviders(response))
    })
  }
}

export function sendRequestAction() {
  return function(dispatch, getState) {
    const state = getState()
    api.logEvent('sendRequest')
    return api.sendRequest(
      state.fields,
      state.select.selectedProvider,
      getSelected(state.select[SERVICES])
    )
  }
}

export function setPageAction(page) {
  if (typeof page === 'string') {
    page = parseInt(page, 10)
  }
  invariant(typeof page === 'number', 'Page must be int: ' + typeof page)
  return function(dispatch, getState) {
    api.logEvent('page-' + page)
    if (page === 2) {
      return dispatch(getPostnummerDataAction()).then(() => {
        dispatch(setField('postPostcode', getState().fields.postnummer))
        dispatch(setField('postPostplace', getState().postnummer.poststed))
        return dispatch(setPage(page))
      })
    } else if (page === 3) {
      dispatch(setPage(page))
      return dispatch(getProviders())
    } else if (page === 4) {
      return dispatch(getProviders()).then(() => {
        dispatch(setPage(page))
      })
    } else if (page === 6) {
      return dispatch(sendRequestAction()).then(result => {
        dispatch(setRequestResult(result))
        dispatch(setPage(page))
      })
    } else {
      return dispatch(setPage(page))
    }
  }
}

export default combineReducers({
  error: errorReducer,
  fields: fieldsReducer,
  page: pageReducer,
  postnummer: postnummerReducer,
  serviceGroups: serviceGroupsReducer,
  providers: providersReducer,
  selectedProviders: selectedProvidersReducer,
  select: selectReducer,
  requestResult: requestResultReducer,
})
