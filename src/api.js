import qs from 'qs'

//  return 'http://oljefri.no/sf' + path
//  return 'http://demo.oljefri.no' + path
//  return 'http://local.oljefri' + path

let logId = Date.now()

export default class Api {
  constructor() {
    this.baseUrl = 'https://admin.oljefri.no/api'
    if (window.location.host === 'demo.oljefri.no') {
      this.baseUrl = 'http://demo.oljefri.no'
    }

    if (
      window.location.host === 'oljefri.dv' ||
      window.location.host === 'localhost:3000'
    ) {
      this.baseUrl = 'http://oljefri.dv'
      this.baseUrl = 'http://localhost:8000/api'
    } else if (window.location.host === 'demo.energismart.no') {
      this.baseUrl = 'https://demo.energismart.no/api'
    }

    this.getUrl = this.getUrl.bind(this)
    this.setUrl = this.setUrl.bind(this)
  }

  getUrl(path) {
    return this.baseUrl + path
  }

  setUrl(url) {
    //console.log('Setting api url to:', url)
    return (this.baseUrl = url)
  }

  logEvent(event) {
    var w = window,
      d = document,
      e = d.documentElement,
      g = d.getElementsByTagName('body')[0],
      x = w.innerWidth || e.clientWidth || g.clientWidth,
      y = w.innerHeight || e.clientHeight || g.clientHeight

    fetch(this.getUrl('/events'), {
      credentials: 'omit',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        event,
        id: logId,
        url: window.location.href,
        screen: `${x}x${y}`,
      }),
    })
    try {
      if (window.gtag && typeof window.gtag === 'function') {
        window.gtag('event', 'Skjema.sendRequest', {
          event_category: 'skjema',
          event_label: event,
          value: event,
        })
      }

      if (window.fbq && typeof window.fbq === 'function') {
        window.fbq('track', 'Energismart_skjema', {
          content_name: event,
          content_category: 'Energismart_skjema',
          content_ids: [event],
        })
      }
    } catch (e) {}
  }

  fetchPostnummerData(postnummer) {
    let url = this.getUrl(
      '/frontendLookups/checkPostcode?postnummer=' + postnummer
    )

    return fetch(url, { credentials: 'omit' })
      .then((response) => {
        return response.json()
      })
      .catch((e) => {
        console.error('Could not run request for ', url, e, e.message)
        throw e
      })
  }

  fetchServices(query) {
    const url = this.getUrl('/schema/fetchServicesFlat?' + qs.stringify(query))
    return fetch(url)
      .then((response) => {
        return response.json()
      })
      .catch((e) => {
        console.error('Could not run request for ', url, e, e.message)
        throw e
      })
  }

  fetchProviders(query) {
    const url = this.getUrl('/schema/providerQuery?' + qs.stringify(query))
    //console.error('URL:', url)
    try {
      return fetch(url)
        .then((response) => {
          return response.json()
        })
        .catch((e) => {
          console.error('Could not run request for ', url, e, e.message)
          throw e
        })
    } catch (e) {
      console.error(e, e.message)
      return null
    }
  }

  sendRequest(fields, selectedProviders, selectedServices) {
    let fieldNames = Object.keys(fields)

    for (var i = 0; i < fieldNames.length; i++) {
      if (typeof fields[fieldNames[i]] === 'string') {
        fields[fieldNames[i]] = fields[fieldNames[i]].trim()
      }
    }

    let data = {
      comment: fields.comment,
      requester: {
        firstname: fields.firstname,
        lastname: fields.lastname,
        postAddress: fields.postAddress,
        postPostcode: fields.postPostcode,
        email: fields.email,
        email_again: fields.email,
        termsAgreed: true,
        phone: fields.phone,
      },
      services_list: selectedServices,
      wantsToBeMember: fields.wantsToBeMember || false,
      requestSite: window.location + '',
      buildingdescription_new: {
        area: fields.area,
        buildingtype_id: fields.buildingType_id,
        yearBuilt: fields.yearBuilt,
      },
      providersSelected: Object.keys(selectedProviders),
    }
    //console.log('posting data:', data)

    return fetch(this.getUrl('/schema/create.json'), {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    }).then((response) => {
      if (window.fbq && typeof window.fbq === 'function') {
        window.fbq('track', 'CompleteRegistration', {
          content_name: 'Energismart_skjema',
        })
      }
      return response.json()
    })
  }
}
