import React from 'react'
import getProviderLogo from './getProviderLogo.js'
import scrollTop from './scrollTop'

/*
{'request':{'id':'3417','firms':[['54','Ildstedet']]}}
 */
export default function Page6(props) {
  const { requestResult, providers } = props

  if (requestResult.errors) {
    return (
      <div id="form_step_6" ref={scrollTop}>
        <h4>Takk for at du vil spare energi!</h4>
        <p>
          Det oppstod desverre en feil når vi forsøkte å lagre forespørselen
          din.
        </p>
        <p>
          Kan du være så vennlig å si i fra til info@oljefri.no så skal vi sikre
          at du både får hjelp og at feilen blir rettet.
        </p>
      </div>
    )
  }

  let firmsMap = {}
  requestResult.request.firms.forEach(function (firm) {
    firmsMap[firm[0]] = true
  })
  let providersInfo = []
  providers.forEach(function (provider) {
    if (firmsMap[provider.id]) {
      providersInfo.push(provider)
    }
  })
  return (
    <div id="form_step_6" ref={scrollTop}>
      <h4>Takk for at du vil spare energi!</h4>
      <p>
        Følgende bedrifter har nå mottatt en forespørsel fra deg. De vil gi deg
        et tilbud på de energitiltakene du ønsker. En del energitiltak krever at
        bedriftene gjennomfører en befaring før de kan gi deg et tilbud. Da vil
        de ta kontakt med deg for å avtale tidspunkt som passer.
      </p>
      <p>
        Energispesialisene vil gi deg et svar så snart de har mulighet, og
        senest innen 14 dager.
      </p>
      <h3>Disse bedriftene har fått din forespørsel:</h3>

      <div className="row small-up-1 medium-up-2 large-up-3">
        {providersInfo.map(function (provider) {
          return (
            <div className="column" key={provider.id}>
              <div className="small-4 columns provider-logo">
                {provider.logo && (
                  <img
                    alt={provider.mainContact.firmname}
                    className="thumbnail"
                    src={getProviderLogo(provider)}
                  />
                )}
              </div>
              <div className="small-8 columns">
                <h5>{provider.mainContact.firmname}</h5>
                {provider.mainContact.phone && (
                  <p>Telefon: {provider.mainContact.phone}</p>
                )}
                {provider.mainContact.email && (
                  <p>E-post: {provider.mainContact.email}</p>
                )}
                <p>
                  <strong
                    style={{ display: 'inline-block', marginRight: '4px' }}>
                    Leverer:
                  </strong>
                  {provider.Tjenester.map(function (service) {
                    return (
                      <span className="label" key={service.id}>
                        {service.name}
                      </span>
                    )
                  })}
                </p>
              </div>
            </div>
          )
        })}
      </div>

      <div className="progress" role="progressbar">
        <div className="progress-meter" style={{ width: '100%' }}>
          100%
        </div>
      </div>
    </div>
  )
}
