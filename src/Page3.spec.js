import React from 'react'
import 'whatwg-fetch'
import { mount } from 'enzyme'

import { buildingTypesFixture } from './fixtures'

import lang from './lang'
import sinon from 'sinon'

import setValue from './setValue'

// import Page1 from '../schema/Page1'
// import Page2, { Page2NoProviders } from '../schema/Page2'
import Page3 from './Page3'
// import Page4 from '../schema/Page4'

describe('Page3', function () {
  let fields
  let page

  function makeProps() {
    fields = {}
    return {
      fields: fields,
      setField: (event) => {
        const f = event.target.getAttribute('data-field')
        const v = event.target.value
        fields[f] = v
        page.setProps({ fields })
      },
      buildingTypes: buildingTypesFixture(),
      setPage: sinon.spy(),
      providers: [{}], // we only care about length
      lang,
    }
  }

  function setup(props) {
    props = props || makeProps()
    return mount(<Page3 {...props} />)
  }

  beforeEach(function () {
    page = setup()
  })
  afterEach(function () {
    page = null
  })
  xit('should tell how many providers are available', function () {})

  it('should go back to page 2 on back', function () {
    page.find('.prev-page').simulate('click')
    let props = page.props()
    expect(props.setPage.called).toBe(true)
    let event = props.setPage.args[0][0]

    expect(event.target.getAttribute('data-page')).toEqual('2')
  })

  it('should list the nr of available providers', function () {
    expect(page.find('h5').first().text()).not.toContain('XX')
    expect(page.find('h5').first().text()).toContain('1')
  })

  it('should allow the user to enter area, buildingType and year', function () {
    setValue(page, '.building-type', '1')

    page.find('.next-page').simulate('click')

    expect(page.props().setPage.called).toBe(false)

    expect(page.find('.form-error').length).toBe(2)

    setValue(page, '.year-built', '1920')
    setValue(page, '.comment', 'c')

    page.find('.next-page').simulate('click')
    expect(page.props().setPage.called).toBe(false)

    expect(page.find('.form-error').length).toBe(1)

    setValue(page, '.area', '120')

    page.find('.next-page').simulate('click')
    expect(page.props().setPage.called).toBe(true)

    expect(page.find('.form-error').length).toBe(0)

    expect(fields['buildingType_id']).toEqual('1')
    expect(fields['yearBuilt']).toEqual('1920')
    expect(fields['area']).toEqual('120')
    expect(fields['comment']).toEqual('c')
  })
})
