import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Progress from './Progress'
import scrollTop from './scrollTop'
import Provider from './Provider'
import { reverseGroupings } from './serviceGroupings'
import BottomButtons from './BottomButtons'
import SelectAllButton from './SelectAllButton'

import './Page4.css'

export default class Page4 extends Component {
  static propTypes = {
    providers: PropTypes.array.isRequired,
    onSelectProvider: PropTypes.func.isRequired,
    selectedProviders: PropTypes.object.isRequired,
    toggledProviders: PropTypes.object.isRequired,
    onToggleProvider: PropTypes.func.isRequired,
    site: PropTypes.oneOf(['energismart', 'oljefri']).isRequired,
  }

  selectAllProviders = () => {
    const { providers, selectedProviders } = this.props
    const numSelected = Object.keys(selectedProviders).length
    if (numSelected === providers.length) {
      this.props.onSelectMany('selectedProvider', [], true)
      return
    }
    const ids = providers.map(p => p.id)
    this.props.onSelectMany('selectedProvider', ids, true)
  }

  render() {
    const {
      providers,
      onSelectProvider,
      selectedProviders,
      toggledProviders,
      onToggleProvider,
      //      lang,
      setPage,
      progress,
      site,
    } = this.props
    let buttonDisabled = Object.keys(selectedProviders).length === 0

    const relevantServices = Object.keys(reverseGroupings[site])
    const numSelected = Object.keys(selectedProviders).length
    return (
      <div ref={scrollTop} className="Page4">
        <h4 className="Page4-header">Velg bedrifter:</h4>
        <div className="Page4-selectall">
          {providers.length > 0 && (
            /*<button
            type="button"
            onClick={this.selectAllProviders}
            className="button button-small of-select-all Page4-selectall">
            Velg alle energispesialister
          </button>*/
            <SelectAllButton
              onClick={this.selectAllProviders}
              isChecked={providers.length === numSelected}>
              Velg alle energispesialister
            </SelectAllButton>
          )}
        </div>
        <button
          className={
            'Page4-next-page-top' +
            (providers && providers.length === numSelected
              ? ' Page4-next-page-top-visible'
              : '')
          }
          onClick={setPage}
          data-page="5"
          disabled={buttonDisabled}
          id="go_to_step_5">
          Legg til informasjon om deg »
        </button>
        {providers.length === 0 && (
          <p>
            <strong>Takk for at du vil finne en energispesialist!</strong>
            <br />
            Vi jobber kontinuerlig med å godkjenne energispesialister. Dessverre
            kan ingen av bedriftene levere disse energitiltakene med din
            bygnings spesifikasjoner.
          </p>
        )}

        <div className="Page4-providers-bg">
          <div className="Page4-providers">
            {providers.map(provider => {
              return (
                <Provider
                  key={provider.id}
                  {...{
                    provider,
                    onSelectProvider,
                    onToggleProvider,
                    selectedProviders,
                    toggledProviders,
                    relevantServices,
                  }}
                />
              )
            })}
          </div>
        </div>
        <BottomButtons
          currentStep={4}
          onNextPage={setPage}
          nextPageError={null}
          nextPageEnabled={!buttonDisabled}
          nextPageLabel="Legg til informasjon om deg"
          setPage={setPage}
        />

        <br />
        <Progress progress={progress} />
      </div>
    )
  }
}
