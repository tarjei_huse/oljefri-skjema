import PropTypes from 'prop-types'
import React, { Component } from 'react'
import scrollTop from './scrollTop'
import BottomButtons from './BottomButtons'

import Progress from './Progress'
import Select from './Select'
import './Page3.css'

import buildingTypes from './buildingTypes'

export default class Page3 extends Component {
  static propTypes = {
    lang: PropTypes.object.isRequired,
    fields: PropTypes.object.isRequired,
    setPage: PropTypes.func.isRequired,
    setField: PropTypes.func.isRequired,
    providers: PropTypes.array,
  }

  constructor(props) {
    super(props)
    this.onClickNext = this.onClickNext.bind(this)
    this.state = { showMessages: false }
  }

  render() {
    const { fields, setField, lang, progress, providers } = this.props
    const { showMessages } = this.state
    //console.log("Render page3", { fields, setField, buildingTypes, lang })
    const areaOptions = {
      '50': 'Under 70 m2',
      '90': '70 - 90 m2',
      '120': '90 - 120 m2',
      '150': '120 - 150 m2',
      '200': '150 - 200 m2',
      '250': '200 - 250 m2',
      '300': 'Større enn 250 m2',
    }

    const yearOptions = {
      '1910': 'Før 1920',
      '1920': '1920 - 1950',
      '1956': '1950 - 1970',
      '1975': '1970 - 1980',
      '1990': '1980 - 1997',
      '2000': '1997 - 2010',
      '2010': '2010 - ',
    }
    let buildingTypeOptions = {}
    buildingTypes
      //.filter((bt) => bt.private)
      .forEach(bt => {
        buildingTypeOptions[bt.id] = bt.name
      })

    return (
      <div id="form_step_3" ref={scrollTop}>
        {providers &&
          providers.length > 0 && (
            <h5>{lang.page3_sub_heading.replace('XX', providers.length)}</h5>
          )}
        <h4>{lang.page3_heading}</h4>
        <p>{lang.page3_subtext}</p>
        <br />
        <form>
          <div className="row">
            <div className="large-3 small-12 columns">
              <label
                className="text-right middle Page3-label"
                htmlFor="middle-label">
                {lang.page3_buildingType_title}{' '}
                <span style={{ color: 'red' }}>*</span>
              </label>
            </div>
            <Select
              fieldName="buildingType_id"
              className="building-type"
              setField={setField}
              fields={fields}
              options={buildingTypeOptions}
              error={
                showMessages &&
                !fields.hasOwnProperty('buildingType_id') &&
                lang.page3_buildingType_error
              }
              placeholder={lang.page3_buildingType_placeholder}
            />
          </div>
          <div className="row">
            <div className="large-3 small-12 columns">
              <label
                className="text-right middle Page3-label"
                htmlFor="middle-label">
                {lang.page3_year_title} <span style={{ color: 'red' }}>*</span>
              </label>
            </div>
            <Select
              fieldName="yearBuilt"
              setField={setField}
              fields={fields}
              options={yearOptions}
              className="year-built"
              error={
                showMessages &&
                !fields.hasOwnProperty('yearBuilt') &&
                lang.page3_yearBuilt_error
              }
              placeholder={lang.page3_yearBuilt_placeholder}
            />
          </div>
          <div className="row">
            <div className="large-3 small-12 columns">
              <label
                className="text-right middle Page3-label"
                htmlFor="middle-label">
                {lang.page3_area_title} <span style={{ color: 'red' }}>*</span>
              </label>
            </div>
            <Select
              fieldName="area"
              setField={setField}
              fields={fields}
              options={areaOptions}
              className="area"
              error={
                showMessages &&
                !fields.hasOwnProperty('area') &&
                lang.page3_area_error
              }
              placeholder={lang.page3_area_placeholder}
            />
          </div>
          <div className="row">
            <div className="small-12 large-9 medium-9 large-offset-3 columns">
              <h5>Ytterligere informasjon:</h5>
              <textarea
                value={fields['comment']}
                name="comment"
                className="small-12 large-9 comment"
                data-field="comment"
                onChange={setField}
              />
            </div>
          </div>
        </form>

        <BottomButtons
          currentStep={3}
          onNextPage={this.onClickNext}
          nextPageError={null}
          nextPageEnabled={true}
          nextPageLabel={lang.page3_next}
          setPage={this.props.setPage}
        />

        <br />
        <Progress progress={progress} />
      </div>
    )
  }

  onClickNext(event) {
    const { fields } = this.props
    if (
      fields.hasOwnProperty('buildingType_id') &&
      fields.hasOwnProperty('yearBuilt') &&
      fields.hasOwnProperty('area')
    ) {
      this.props.setPage(event)
    } else {
      this.setState({ showMessages: true })
    }
  }
}

/**
 * Funksjoner som ikke er i bruk i v1.
 * @param {[type]} props [description]
 */
/*
function RenderExtraEnergyOptions(props) {
  return <div className='row'>
        <div className='small-3 columns'>
          &nbsp;
        </div>
        <div className='small-9 columns'>
          <h5>Velg nåværende oppvarming:</h5>
          <input type='checkbox' id='checkbox3_1_1' /><label htmlFor='checkbox3_1_1'>Elektrisistet</label><br />
          <div id='checkbox3_1_1_subinfo' className='subinfo'>
            <p><strong>Energiforbruk:</strong></p>
            <div className='row'>
              <div className='small-6 columns'>
                <input type='text' placeholder id='middle-label' />
              </div>
              <div className='small-6 columns'>
                <label className='middle' htmlFor='middle-label'>kwh</label>
              </div>
            </div>
          </div>
          <input type='checkbox' id='checkbox3_1_2' /><label htmlFor='checkbox3_1_2'>Olje</label><br />
          <div id='checkbox3_1_2_subinfo' className='subinfo'>
            <p><strong>Energiforbruk:</strong></p>
            <div className='row'>
              <div className='small-6 columns'>
                <input type='text' placeholder id='middle-label' />
              </div>
              <div className='small-6 columns'>
                <label className='middle' htmlFor='middle-label'>liter</label>
              </div>
            </div>
          </div>
          <input type='checkbox' id='checkbox3_1_3' /><label htmlFor='checkbox3_1_3'>Parafin</label><br />
          <div id='checkbox3_1_3_subinfo' className='subinfo'>
            <p><strong>Energiforbruk:</strong></p>
            <div className='row'>
              <div className='small-6 columns'>
                <input type='text' placeholder id='middle-label' />
              </div>
              <div className='small-6 columns'>
                <label className='middle' htmlFor='middle-label'>liter</label>
              </div>
            </div>
          </div>
          <input type='checkbox' id='checkbox3_1_4' /><label htmlFor='checkbox3_1_4'>Ved</label><br />
          <input type='checkbox' id='checkbox3_1_5' /><label htmlFor='checkbox3_1_5'>Varmepumpe</label><br />
          <input type='checkbox' id='checkbox3_1_6' /><label htmlFor='checkbox3_1_6'>Annet</label><br />
          <div id='checkbox3_1_6_subinfo' className='subinfo'>
            <p><strong>Forbruk/pris:</strong></p>
            <div className='row'>
              <div className='small-6 columns'>
                <input type='text' placeholder id='middle-label' />
              </div>
              <div className='small-6 columns'>
                <label className='middle' htmlFor='middle-label' />
              </div>
            </div>
          </div>
          <h5>Ytterligere informasjon:</h5>
          <textarea defaultValue={""} />
        </div>
      </div>
}
*/
