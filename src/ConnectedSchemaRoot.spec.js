import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import setValue from './setValue'
import { mount } from 'enzyme'

import { Provider } from 'react-redux'

import { providerQueryResponse } from './fixtures'

import reducers from './store'

import Page1 from './Page1'
import Page4 from './Page4'
import SchemaRoot from './SchemaRoot'

describe('ConnectedRoot', function () {
  let mocks

  afterEach(function () {
    fetchMock.restore()
  })

  function mockEvents() {
    mocks.mock(/\/events/, {
      status: 200,
      body: '',
    })
  }

  function createStoreForPage(page) {
    return createStore(
      reducers,
      {
        providers: providerQueryResponse(),
        page,
        select: {
          toggledProvider: {},
          selectedProvider: {},
        },
      },
      applyMiddleware(thunk)
    )
  }

  it('should handle startup', function () {
    const store = createStoreForPage(1)
    let page = mount(
      <Provider store={store}>
        <SchemaRoot postnummer={'9010'} site="energismart" />
      </Provider>
    )

    expect(page.find(Page1).length).toBe(1)
  })

  it('should handle page 4', function () {
    const store = createStoreForPage(4)

    let page = mount(
      <Provider store={store}>
        <SchemaRoot site="oljefri" postnummer={'9010'} />
      </Provider>
    )

    expect(page.find(Page4).length).toBe(1)
  })

  describe('page3- page 7', function () {
    beforeEach(function () {
      mocks = fetchMock
        .mock(/\/schema\/providerQuery\?.+/, {
          status: 200,
          body: providerQueryResponse(),
        })
        .catch(404)
      mockEvents()
    })
    function setPage3Values(page) {
      setValue(page, '.building-type', '1')
      setValue(page, '.year-built', '1920')
      setValue(page, '.comment', 'c')
      setValue(page, '.area', '120')
    }

    it.skip('should post to the backend', function () {
      const store = createStoreForPage(3)
      let page = mount(
        <Provider store={store}>
          <SchemaRoot site="oljefri" postnummer={'9010'} />
        </Provider>
      )

      setPage3Values(page)

      page.find('.next-page').simulate('click')

      return mocks
        .flush()
        .then(returnPromise(page, mocks))
        .then(() => {
          expect(page.find('.provider-checkbox').length).toBe(10)

          page.find('.provider-checkbox').first().simulate('click')

          page.find('.next-page').simulate('click')

          page.update()

          //   console.log('page', page.debug())
          expect(page.find('.Page5').length).toEqual(1)
        })
    })
  })
})

function returnPromise(page, mocks) {
  mocks.flush()
  page.update()
  return Promise.resolve(true)
}
