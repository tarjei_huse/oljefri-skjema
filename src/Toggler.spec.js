import React from 'react'
import { mount } from 'enzyme'
import Toggle from './Toggle'

describe('Toggle', function() {
  let page
  let props

  function makeProps() {
    return {}
  }

  function setup(props) {
    props = props || makeProps()
    return mount(
      <Toggle {...props}>
        {function(toggled, onToggle) {
          return (
            <div
              className={toggled ? 'toggled' : 'untoggled'}
              onClick={onToggle}>
              tt
            </div>
          )
        }}
      </Toggle>
    )
  }

  beforeEach(function() {
    props = makeProps()
    page = setup(props)
  })

  afterEach(function() {
    props = page = null
  })

  it('should show untoggled by default', function() {
    expect(page.find('.untoggled').length).toBe(1)

    page.simulate('click')
    expect(page.find('.untoggled').length).toBe(0)
    expect(page.find('.toggled').length).toBe(1)

    page.simulate('click')

    expect(page.find('.untoggled').length).toBe(1)
  })
})
