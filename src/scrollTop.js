export default function scrollTop(dom) {
  if (dom && dom.scrollIntoView) {
    const elm = document.getElementById('showform')
    if (elm && elm.scrollIntoView) {
      elm.scrollIntoView({ behavior: 'smooth', block: 'start' })
    }
  }
}
