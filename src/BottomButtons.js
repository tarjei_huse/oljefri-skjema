import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './BottomButtons.css'

export default class BottomButtons extends Component {
  static propTypes = {
    currentStep: PropTypes.number.isRequired,
    nextPageEnabled: PropTypes.bool.isRequired,
    nextPageError: PropTypes.string,
    nextPageLabel: PropTypes.string.isRequired,
    onNextPage: PropTypes.func.isRequired,
    setPage: PropTypes.func.isRequired,
  }
  static defaultProps = {
    nextPageError: null,
  }

  render() {
    const {
      onNextPage,
      currentStep,
      nextPageError,
      nextPageEnabled,
      nextPageLabel,
      setPage,
    } = this.props

    return (
      <React.Fragment>
        {nextPageError && (
          <div className="BottomButtons-error">{nextPageError}</div>
        )}
        <div className="BottomButtons">
          <button
            type="button"
            className="BottomButtons-prev BottomButtons-button prev-page"
            onClick={setPage}
            data-page={currentStep - 1}
            id={`to_step_${currentStep - 1}`}>
            « Tilbake
          </button>
          <button
            className="BottomButtons-button-next next-page BottomButtons-button"
            onClick={onNextPage}
            disabled={!nextPageEnabled}
            data-page={currentStep + 1}
            type="button">
            {nextPageLabel} »
          </button>
        </div>
      </React.Fragment>
    )
  }
}
