import React from 'react'
import 'whatwg-fetch'
import { mount } from 'enzyme'

import { buildingTypesFixture, providerQueryResponse } from './fixtures'

import lang from './lang'
import sinon from 'sinon'
import setValue from './setValue'
import Page5, { doValidation } from './Page5'

describe('Page5', function () {
  let fields
  let page
  let toggledProviders

  function makeProps() {
    fields = {}
    toggledProviders = {
      '41': true,
    }
    return {
      fields: fields,
      setField: (event) => {
        const f = event.target.getAttribute('data-field')
        let v = event.target.value
        if (event.target.type.toLowerCase() === 'checkbox') {
          v = event.target.checked
        }
        fields[f] = v
        page.setProps({ fields })
      },
      buildingTypes: buildingTypesFixture(),
      providers: providerQueryResponse(),
      selectedProviders: {},
      toggledProviders: toggledProviders,
      onToggleProvider: sinon.spy(),
      setPage: sinon.spy(),
      progress: 95,
    }
  }

  function setup(props) {
    props = props || makeProps()
    return mount(<Page5 {...props} />)
  }

  beforeEach(function () {
    page = setup()
    //node = findDOMNode(page.instance())
  })
  afterEach(function () {
    page = null
  })

  it('should let the user enter data-field', function () {
    setValue(page, '#firstname', 'Haralt')
    setValue(page, '#lastname', 'Pra')
    setValue(page, '#adress', 'Got 1')
    setValue(page, '#postnr', '9899')
    setValue(page, '#postplace', 'Alta')
    setValue(page, '#phone', '92039020')
    setValue(page, '#email', 'ema@google.com')
    setValue(page, '#wantsToBeMember', true)
    setValue(page, '#termsAgreed', true)

    expect(fields['firstname']).toEqual('Haralt')
    expect(fields['lastname']).toEqual('Pra')
    expect(fields['postAddress']).toEqual('Got 1')
    expect(fields['postPostcode']).toEqual('9899')
    expect(fields['phone']).toEqual('92039020')
    expect(fields['email']).toEqual('ema@google.com')
    expect(fields['wantsToBeMember']).toEqual(true)
    expect(fields['termsAgreed']).toEqual(true)
  })

  it('should require valid email', function () {
    setValue(page, '#firstname', '')
    setValue(page, '#lastname', '')
    setValue(page, '#adress', '')
    setValue(page, '#postnr', '')
    setValue(page, '#postplace', '')
    setValue(page, '#phone', '99020')
    setValue(page, '#email', 'ema.com')
    setValue(page, '#wantsToBeMember', false)
    setValue(page, '#termsAgreed', false)

    //console.log(JSON.stringify(doValidation(fields)))
    let valRes = doValidation(fields)
    expect(valRes).toEqual({
      firstname: [lang.page5_error_firstname],
      lastname: [lang.page5_error_lastname],
      postAddress: [lang.page5_error_postAddress],
      postPostcode: [lang.page5_error_postPostcode],
      phone: [],
      email: [lang.page5_error_email],
      termsAgreed: [lang.page5_error_termsAgreed],
    })
  })

  it('Validates before submit', function () {
    expect(page.find('.next-page').props().disabled).toBe(true)
    /*
          setValue(page, '#firstname', 'Haralt')
          setValue(page, '#lastname', 'Pra')
          setValue(page, '#adress', 'Got 1')
          setValue(page, '#postnr', '9899')
          setValue(page, '#postplace', 'Alta')
          setValue(page, '#phone', '92039020')
          setValue(page, '#email', 'ema')
          setValue(page, '#wantsToBeMember', true)
          setValue(page, '#termsAgreed', true)
      */
    fields['firstname'] = 'Haralt'
    fields['lastname'] = 'Pra'
    fields['postAddress'] = 'Got 1'
    fields['postPostcode'] = '9899'
    fields['phone'] = '92039020'
    fields['email'] = 'eoogle.com'
    fields['wantsToBeMember'] = true
    fields['termsAgreed'] = true

    page.setProps({ fields })

    expect(page.find('.next-page').props().disabled).toBe(false)

    page.find('.next-page').simulate('click')

    expect(page.find('.error').length).toBe(1)

    expect(page.state().errors).toBeDefined()
    expect(page.state().errors['email']).toBeDefined()
    fields['email'] = 'ema@google.com'
    page.setProps({ fields })

    page.find('.next-page').simulate('click')

    expect(page.find('.error').length).toBe(0)

    expect(page.props().setPage.called).toBe(true)

    //expect(page.props().setPage.args[0][0]).toEqual('6')
  })

  it('Validates to ok before submit', function () {
    expect(page.find('.next-page').props().disabled).toBe(true)
    /*
          setValue(page, '#firstname', 'Haralt')
          setValue(page, '#lastname', 'Pra')
          setValue(page, '#adress', 'Got 1')
          setValue(page, '#postnr', '9899')
          setValue(page, '#postplace', 'Alta')
          setValue(page, '#phone', '92039020')
          setValue(page, '#email', 'ema')
          setValue(page, '#wantsToBeMember', true)
          setValue(page, '#termsAgreed', true)
      */
    fields['firstname'] = 'Haralt'
    fields['lastname'] = 'Pra'
    fields['postAddress'] = 'Got 1'
    fields['postPostcode'] = '9899'
    fields['phone'] = '92039020'
    fields['email'] = 'ee@eoogle.com'
    fields['wantsToBeMember'] = true
    fields['termsAgreed'] = true

    page.setProps({ fields })

    page.find('.next-page').simulate('click')

    expect(page.find('.error').length).toBe(0)

    expect(page.props().setPage.called).toBe(true)
    expect(
      page.props().setPage.args[0][0].target.getAttribute('data-page')
    ).toEqual('6')
  })
})
