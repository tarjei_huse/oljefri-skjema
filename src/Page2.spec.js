import React from 'react'
import 'whatwg-fetch'
import { mount } from 'enzyme'
import { fetchServicesFlatResponse } from './fixtures'
import lang from './lang'
import sinon from 'sinon'
import Page2 from './Page2'

describe('Page2', function () {
  let fields
  let page
  let props

  function makeProps() {
    fields = {}
    return {
      fields: fields,
      setField: (event) => {
        const f = event.target.getAttribute('data-field')
        const v = event.target.value
        fields[f] = v
        page.setProps({ fields })
      },
      postnummer: {
        providerCount: 12,
      },
      progress: 45,
      services: fetchServicesFlatResponse(),
      selectedServices: {},
      onSelectService: sinon.spy(),
      onSelectMany: sinon.spy(),
      setPage: sinon.spy(),
      site: 'oljefri',
      lang,
    }
  }

  function setup(props) {
    props = props || makeProps()
    return mount(<Page2 {...props} />)
  }

  beforeEach(function () {
    props = makeProps()
    page = setup(props)
  })

  afterEach(function () {
    props = page = null
  })

  it('should require a selected firm to continue', function () {
    expect(page.find('.BottomButtons-error').length).toEqual(0)

    page.find('.SelectAllButton').simulate('click')
    expect(page.find('.BottomButtons-error').length).toEqual(0)

    page.find('.SelectAllButton').simulate('click')
    expect(page.find('.BottomButtons-error').length).toEqual(0)

    page.find('.next-page').simulate('click')
    expect(page.find('.BottomButtons-error').length).toBeGreaterThan(0)
  })

  it('should require a selected firm to continue', function () {
    page.setProps({
      selectedServices: {
        13: false,
      },
    })

    page.find('.next-page').simulate('click')

    expect(page.find('.BottomButtons-error').length).toBeGreaterThan(0)
  })

  it('should select all firms on toggle', function () {
    page.find('.SelectAllButton').simulate('click')

    expect(page.props().onSelectMany.called).toEqual(true)
    expect(page.props().onSelectMany.args[0]).toEqual([
      'service',
      [
        '13',
        '14',
        '15',
        '9',
        '17',
        '22',
        '3',
        '23',
        '24',
        '11',
        '5',
        '28',
        '29',
        '30',
        '31',
      ],
      true,
    ])
  })

  it('should not show servicegroups without services', function () {
    props = makeProps()
    props.services = props.services.filter((service) => {
      return [5, 6, 9, 13].indexOf(parseInt(service.id, 10)) === -1
    })
    page = setup(props)

    expect(page.find('.Page2-ServicesGroup').length).toBe(2)
    expect(page.find('.Page2-ServicesGroup').first().find('h3').text()).toEqual(
      'Erstatt oljekjelen'
    )

    expect(page.find('.Page2-ServicesGroup').at(1).find('h3').text()).toEqual(
      'Andre energitiltak'
    )
  })

  it('should show a different organising for energismart.no', function () {
    page.setProps({ site: 'energismart' })

    expect(page.find('.Page2-ServicesGroup').length).toBe(3)
    expect(
      page.find('.Page2-ServicesGroup').first().find('.Page2-service-type')
        .length
    ).toBe(9)
    expect(page.find('.Page2-ServicesGroup').first().find('h3').text()).toEqual(
      'Oppvarming'
    )
    expect(
      page.find('.Page2-ServicesGroup').at(2).find('.Page2-service-type').length
    ).toBe(2)
    expect(
      page.find('.Page2-ServicesGroup').at(2).find('.Page2-service-type').length
    ).toBe(2)

    expect(page.find('.Page2-ServicesGroup').at(1).find('h3').text()).toEqual(
      'Rehabilitering'
    )
  })

  it('should show a different organising for oljefri.no', function () {
    page.setProps({ site: 'oljefri' })

    expect(page.find('.Page2-ServicesGroup').length).toBe(4)
    expect(
      page.find('.Page2-ServicesGroup').first().find('.Page2-service-type')
        .length
    ).toBe(1)
    expect(page.find('.Page2-ServicesGroup').at(1).find('h3').text()).toEqual(
      'Erstatt oljekjelen'
    )
    expect(
      page.find('.Page2-ServicesGroup').at(1).find('.Page2-service-type').length
    ).toBe(6)
    expect(
      page.find('.Page2-ServicesGroup').at(2).find('.Page2-service-type').length
    ).toBe(2)

    expect(page.find('.Page2-ServicesGroup').at(0).find('h3').text()).toEqual(
      'Fjern oljetanken'
    )
  })

  it('should show tooltips', function () {
    expect(page.find('.Page2-has-tip').length).toBe(13)

    page.find('.Page2-service-type .Page2-has-tip').at(2).simulate('click')
    expect(page.find('.Page2-service-tip').length).toBe(1)
  })

  it('should list out all serviceTypes', function () {
    expect(page.find('.Page2-service-type').length).toBe(15)
    expect(page.find('.service-type-checkbox').length).toBe(15)
  })

  it('should handle form key navi', function () {
    expect(page.find('form').length).toBe(1)

    page.find('form').simulate('submit')
    expect(page.find('.BottomButtons-error').length).toBe(1)

    page.setProps({ selectedServices: { 12: true } })

    expect(page.find('.form-error').length).toBe(0)

    page.find('form').simulate('submit')

    expect(props.setPage.called).toBe(true)
  })
})
