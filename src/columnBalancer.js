const permutator = inputArr => {
  let result = []

  const permute = (arr, m = []) => {
    if (arr.length === 0) {
      result.push(m)
    } else {
      for (let i = 0; i < arr.length; i++) {
        let curr = arr.slice()
        let next = curr.splice(i, 1)
        permute(curr.slice(), m.concat(next))
      }
    }
  }

  permute(inputArr)

  return result
}

/**
 * Defines a set of balanced columns
 * @param  {array of {id, weight}} items list of items and their weight
 * @param  {integer} nr of columns
 * @return {[type]}       list of item ids
 */
export function columnBalancer(items, numColums) {
  let solution = null

  const weightMap = {}
  items.forEach(item => {
    weightMap[item.id] = item.weight
  })

  const perms = permutator(Object.keys(weightMap))

  let minWeight = 500 // should be integer.max
  for (let i = 0; i < perms.length; i += 1) {
    let currColumn = 0
    let columns = []
    let currentPermutation = perms[i]
    // compute weight of each column
    for (
      var permIndex = 0;
      permIndex < currentPermutation.length;
      permIndex++, currColumn++
    ) {
      if (currColumn >= numColums) {
        currColumn = 0
      }
      columns[currColumn] = columns[currColumn] || 0
      let id = currentPermutation[permIndex]
      columns[currColumn] += weightMap[id]
    }
    const currentWeight = Math.max(...columns)
    if (currentWeight < minWeight) {
      solution = currentPermutation
      minWeight = currentWeight
    }
  }
  return solution
}
