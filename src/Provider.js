import React, { Component } from 'react'
import getProviderLogo from './getProviderLogo.js'
import './Provider.css'

export default class Provider extends Component {
  render() {
    const {
      provider,
      onSelectProvider,
      selectedProviders,
      relevantServices,
    } = this.props
    const isSelected = selectedProviders[provider.id] || false
    const logo = getProviderLogo(provider)
    return (
      <div className={'Provider provider-' + provider.id}>
        <div className="Provider-logo">
          {provider.logo && (
            <img alt={provider.mainContact.firmname} src={logo} />
          )}
        </div>
        <div className="Provider-blurb">
          <label
            htmlFor={'provider_checkbox_' + provider.id}
            className={`Provider-label ${
              isSelected ? 'Provider-label-selected' : ''
            } `}>
            <input
              type="checkbox"
              id={'provider_checkbox_' + provider.id}
              onChange={onSelectProvider}
              checked={isSelected}
              className="provider-checkbox"
              data-field={'selectedProviders'}
              value={provider.id}
            />
            <div>{provider.mainContact.firmname}</div>
          </label>
          <div className="Provider-description">{provider.description}</div>
          <div className="Provider-services-heading">Leverer:</div>
          <div className="Provider-services">
            {provider.Tjenester.map(function (service) {
              if (relevantServices.indexOf(service.id) === -1 && false) {
                return null
              }
              return (
                <span className="Provider-service" key={service.id}>
                  {service.name}
                </span>
              )
            })}
          </div>
        </div>
      </div>
    )
  }
}
