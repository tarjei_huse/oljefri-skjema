import PropTypes from 'prop-types'
import React, { Component } from 'react'
import scrollTop from './scrollTop'
import Progress from './Progress'
import lang from './lang'
import Toggle from './Toggle'
import BottomButtons from './BottomButtons'

function focus(d) {
  if (d) {
    d.focus()
  }
}
//let regExp = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
var validationMethods = {
  presence: function(key, value) {
    return typeof value !== 'undefined' && !!value
  },

  mail: function(key, value) {
    //    return ! (false === /\S+@\S+/i.test(value))
    value = value && value.trim()
    if (!value) {
      return false
    }

    if (value.indexOf('@') < 2) {
      return false
    }

    if (value.indexOf('@') === value.length - 1) {
      return false
    }
    return true
  },
  trueVal: function(key, value) {
    return !!value
  },
}
const constraints = {
  firstname: {
    presence: { message: lang.page5_error_firstname },
  },
  lastname: {
    presence: { message: lang.page5_error_lastname },
  },
  postAddress: {
    presence: { message: lang.page5_error_postAddress },
  },
  postPostcode: {
    presence: { message: lang.page5_error_postPostcode },
  },
  phone: {
    presence: { message: lang.page5_error_phone },
  },
  email: {
    presence: { message: lang.page5_error_email },
    mail: { message: lang.page5_error_email },
  },
  termsAgreed: {
    trueVal: {
      message: lang.page5_error_termsAgreed,
    },
  },
}

export function doValidation(fields) {
  let result = {}
  let hasErrors = false
  const validationFields = Object.keys(constraints)

  for (var i = 0; i < validationFields.length; i++) {
    const key = validationFields[i]
    if (!constraints.hasOwnProperty(key)) {
      console.error('Unknown constraint:', key)
      return
    }
    result[key] = []
    let fieldConstraints = Object.keys(constraints[key])
    for (var j = 0; j < fieldConstraints.length; j++) {
      let constraint = fieldConstraints[j]
      if (!validationMethods.hasOwnProperty(constraint)) {
        console.error('unknown validation method:', constraint)
      }

      if (!validationMethods[constraint](key, fields[key])) {
        result[key].push(constraints[key][constraint].message)
        hasErrors = true
      }
    }
  }

  if (!hasErrors) {
    return false
  }
  return result
}

function Field(props) {
  const error =
    (props.errors &&
      Array.isArray(props.errors[props['data-field']]) &&
      props.errors[props['data-field']][0]) ||
    false
  const label = props.label || null
  let classes = ['field-' + props.id]
  if (error) {
    classes.push('error')
  }

  let inputProps = Object.assign({}, props)
  delete inputProps.errors
  delete inputProps.autofill
  delete inputProps.label

  if (inputProps._ref) {
    inputProps.ref = inputProps._ref
    delete inputProps._ref
  }

  return (
    <div className={classes.join(' ')}>
      <input {...inputProps} />
      {label}
      {error && (
        <div className="form-error" style={{ display: 'block' }}>
          {error}
        </div>
      )}
    </div>
  )
}

export default class Page5 extends Component {
  static propTypes = {
    progress: PropTypes.number.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = { buttonToggled: false, errors: null }
    this.toggleButton = this.toggleButton.bind(this)
    this.execValidate = this.execValidate.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  render() {
    const { setField, fields, progress } = this.props
    const { errors } = this.state

    let formInValid = Object.keys(fields).length < 3

    //console.log('FF', formInValid, Object.keys(fields).length < 3 , !errors)

    return (
      <div id="form_step_5" ref={scrollTop}>
        <h4>Og til slutt, litt om deg:</h4>
        <form>
          {errors && (
            <div className="alert callout">
              <p>
                Det er flere verdier under som er feil eller som må fylles ut!
              </p>
            </div>
          )}
          <div className="row">
            <div className="small-3 columns">
              <label className="text-right middle" htmlFor="firstname">
                Fornavn:
                <span style={{ color: 'red' }}>*</span>
              </label>
            </div>
            <div className="small-9 columns">
              <Field
                type="text"
                id="firstname"
                _ref={focus}
                errors={errors}
                autofill="given-name"
                required
                placeholder="Fornavn"
                onChange={setField}
                value={fields['firstname'] || ''}
                data-field="firstname"
              />
            </div>
          </div>
          <div className="row">
            <div className="small-3 columns">
              <label className="text-right middle" htmlFor="lastname">
                Etternavn:
                <span style={{ color: 'red' }}>*</span>
              </label>
            </div>
            <div className="small-9 columns">
              <Field
                type="text"
                autofill="family-name"
                errors={errors}
                id="lastname"
                required
                placeholder="Etternavn"
                onChange={setField}
                value={fields['lastname'] || ''}
                data-field="lastname"
              />
            </div>
          </div>
          <div className="row">
            <div className="small-3 columns">
              <label className="text-right middle" htmlFor="adress">
                Adresse:
                <span style={{ color: 'red' }}>*</span>
              </label>
            </div>
            <div className="small-9 columns">
              <Field
                type="text"
                required
                placeholder="Gatenavn nr 1"
                onChange={setField}
                autoComplete="shipping street-address"
                value={fields['postAddress'] || ''}
                data-field="postAddress"
                errors={errors}
                id="adress"
              />
            </div>
          </div>
          <div className="row">
            <div className="small-3 columns">
              <label className="text-right middle" htmlFor="postnr">
                Postnummer:
              </label>
            </div>
            <div className="small-9 columns">
              <Field
                type="text"
                placeholder="postnummer"
                id="postnr"
                errors={errors}
                autoComplete="shipping postal-code"
                onChange={setField}
                value={fields['postPostcode'] || ''}
                data-field="postPostcode"
              />
            </div>
          </div>
          <div className="row">
            <div className="small-3 columns">
              <label className="text-right middle" htmlFor="postplace">
                Poststed:
              </label>
            </div>
            <div className="small-9 columns">
              <Field
                type="text"
                placeholder="Poststed"
                id="postplace"
                onChange={setField}
                errors={errors}
                data-field="postPostplace"
                autoComplete="shipping country-name"
                value={fields['postPostplace'] || ''}
              />
            </div>
          </div>
          <h4>Kontakt meg på:</h4>
          <div className="row">
            <div className="small-3 columns">
              <label className="text-right middle" htmlFor="phone">
                Telefon:
                <span style={{ color: 'red' }}>*</span>
              </label>
            </div>
            <div className="small-9 columns">
              <Field
                type="phone"
                placeholder="999 99 999"
                id="phone"
                onChange={setField}
                data-field="phone"
                autoComplete="tel mobile"
                errors={errors}
                value={fields['phone'] || ''}
              />
            </div>
          </div>
          <div className="row">
            <div className="small-3 columns">
              <label className="text-right middle" htmlFor="email">
                E-post:
                <span style={{ color: 'red' }}>*</span>
              </label>
            </div>
            <div className="small-9 columns">
              <Field
                type="email"
                placeholder="din@epost.com"
                id="email"
                onChange={setField}
                value={fields['email'] || ''}
                data-field="email"
                autoComplete="email"
                errors={errors}
              />
            </div>
          </div>
          <br />
          <Field
            type="checkbox"
            id="termsAgreed"
            onChange={setField}
            errors={errors}
            value={fields['termsAgreed'] || ''}
            data-field="termsAgreed"
            label={
              <label htmlFor="termsAgreed" style={{ verticalAlign: 'top' }}>
                <Toggle>
                  {function(toggled, onToggle) {
                    return (
                      <div onClick={onToggle} style={{ cursor: 'pointer' }}>
                        <span style={{ color: 'red' }}>*</span> Jeg aksepterer
                        <span
                          title="trykk her for å se vilkårene"
                          tabIndex={1}
                          data-disable-hover="false"
                          className="has-tip"
                          aria-haspopup="true"
                          data-tooltip>
                          &nbsp;vilkårene.
                        </span>
                        {toggled && (
                          <div style={{ margin: '2rem' }}>
                            <h2>Vilkår for bruk</h2>
                            <p>
                              1. Dette er en formidlingstjeneste og en
                              invitasjon til en felles dugnad for å kutte olje-
                              og energiforbruket i Norge. Naturvernforbundet er
                              ansvarlig for innholdet på nettsidene.
                            </p>
                            <p>
                              2. Det er helt uforpliktende for deg å bruke
                              forespørselsskjemaet "Finn Energispesialister".
                              Samtidig håper vi at du har et reelt ønske om å
                              gjennomføre klima- og energitiltak, og at du vil
                              ende opp med å gjennomføre viktige klima- og
                              energismarte energitiltak.
                            </p>
                            <p>
                              3. De bedriftene som vil besvare forespørselen din
                              er tatt opp i ordningen "Energispesialist", og
                              oppfylte kravene for opptak til ordningen på
                              godkjenningstidspunktet. Du finner mer informasjon
                              om ordningen i hovedmenypunktet
                              "Energispesialister" på nettsiden.
                            </p>
                            <p>
                              Vi oppfordrer deg likevel til å gjøre egne
                              undersøkelser før du inngår en forpliktende avtale
                              med en bedrift.
                            </p>
                            <p>
                              4. Naturvernforbundet har verken økonomisk eller
                              juridisk ansvar som formidler av kontakt hvis det
                              oppstår en konflikt mellom deg og en av bedriftene
                              som er tatt opp som Energispesialist. Oppstår det
                              tvister må du gjerne orientere oss, men slike
                              saker meldes til, og avgjøres i
                              Forbrukertvistutvalget.
                            </p>
                            <p>
                              5. Naturvernforbundet vil gjerne ha tilbakemelding
                              på både gode og dårlige erfaringer med nettsiden
                              eller bedriftene. Derfor kan det hende vi sender
                              deg én eller to spørreundersøkelser om dette på
                              e-post.
                            </p>
                            <p>
                              6. Vi tar personvernet ditt alvorlig. Derfor
                              lagrer vi bare informasjonen du sender inn via
                              dette skjemaet i ett år før alle personlige data
                              slettes. Fordi vi gjerne vil vite om, og når
                              innsatsen vår har effekt, bruker vi cookies for å
                              spore dette, særlig i forbindelse med
                              markedsføring. Du finner mer informasjon om
                              cookies i Personvern og Informasjonskapsler som du
                              finner helt nederst på nettsiden.
                            </p>
                          </div>
                        )}
                      </div>
                    )
                  }}
                </Toggle>
              </label>
            }
          />
          <br />
          <Field
            type="checkbox"
            id="wantsToBeMember"
            onChange={setField}
            label={
              <label htmlFor="wantsToBeMember">
                Jeg vil bli medlem i Naturvernforbundet (kun 200,- første år).
              </label>
            }
            value={fields['wantsToBeMember'] || ''}
            data-field="wantsToBeMember"
          />

          <br />
          <br />

          <ul
            style={{ marginTop: '-1.6em', width: '90.2%' }}
            role="tablist"
            className="accordion">
            <Toggle>
              {function(toggled, onToggle) {
                return (
                  <li className="accordion-item">
                    <button
                      aria-controls="panel1d"
                      type="button"
                      onClick={onToggle}
                      id="panel1d-heading"
                      style={{ width: '100%' }}
                      className="accordion-title"
                      role="tab">
                      <h4>Fortell meg mer om Naturvernforbundet</h4>
                    </button>
                    {toggled && (
                      <div
                        aria-labelledby="panel1d-heading"
                        style={{ display: 'block' }}
                        role="tabpanel"
                        className="accordion-content"
                        id="panel1d">
                        <p>
                          <strong>
                            Tusen takk for at du ønsker å bli energismart!
                          </strong>
                        </p>
                        <p>
                          Energi er en fantastisk ressurs, men det er miljøet
                          vårt som betaler prisen når vi bruker energi fra
                          fossile kilder, og når vi bruker mer energi enn vi
                          trenger.
                        </p>
                        <p>
                          Naturvernforbundet er Norges eldste natur- og
                          miljøvernorganisasjon. Vi jobber med mange saker innen
                          miljø- og naturvern, men spesielt med områdene
                          naturvern, klima, energi, samferdsel og miljøvennlig
                          forbruk.
                        </p>
                        <p>
                          Naturvernforbundet er demokratisk, med medlemmer og
                          lokal- og fylkeslag i hele landet. Vi ønsker å gjøre
                          det lettere å leve klima- og energismart.
                        </p>
                        <p>
                          <strong>
                            Vi vil gjerne ha deg med på laget – bli medlem du
                            også!{' '}
                          </strong>
                        </p>
                      </div>
                    )}
                  </li>
                )
              }}
            </Toggle>
          </ul>
          <BottomButtons
            currentStep={5}
            onNextPage={this.onSubmit}
            nextPageError={null}
            nextPageEnabled={!formInValid}
            nextPageLabel="Send inn"
            setPage={this.props.setPage}
          />
          <br />
          <Progress progress={progress} />
        </form>
      </div>
    )
  }

  onSubmit(event) {
    event.stopPropagation()
    event.preventDefault()
    if (!this.execValidate()) {
      this.props.setPage(event)
    }
  }
  toggleButton() {
    this.setState({ buttonToggled: !this.state.buttonToggled })
  }

  execValidate() {
    const errors = doValidation(this.props.fields)
    //console.log('errros', errors)
    this.setState({ errors: errors })
    return errors
  }
}
