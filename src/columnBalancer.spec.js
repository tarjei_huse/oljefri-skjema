import { columnBalancer } from './columnBalancer'

describe('columnBalancer', function() {
  it('should find the correct combo of a single column', function() {
    const weights = [{ id: 1, weight: 3 }]

    expect(columnBalancer(weights, 1)).toEqual(['1'])
    expect(columnBalancer(weights, 2)).toEqual(['1'])
    expect(columnBalancer(weights, 3)).toEqual(['1'])
  })

  it('should handle weights of multiple columns', function() {
    const weights = [
      { id: 2, weight: 2 },
      { id: 1, weight: 3 },
      { id: 3, weight: 1 },
    ]

    expect(columnBalancer(weights, 1)).toEqual(['1', '2', '3'])
    expect(columnBalancer(weights, 2)).toEqual(['2', '1', '3'])
    expect(columnBalancer(weights, 3)).toEqual(['1', '2', '3'])
  })

  it('should find the correct combo of a single column', function() {
    const weights = [
      { id: 2, weight: 2 },
      { id: 1, weight: 3 },
      { id: 3, weight: 1 },
      { id: 4, weight: 1 },
      { id: 5, weight: 1 },
    ]

    expect(columnBalancer(weights, 1)).toEqual(['1', '2', '3', '4', '5'])
    expect(columnBalancer(weights, 2)).toEqual(['2', '1', '3', '4', '5'])
    expect(columnBalancer(weights, 3)).toEqual(['2', '3', '1', '4', '5'])
  })
})
