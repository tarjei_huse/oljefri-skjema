//import React from 'react'
//import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
//import sinon from 'sinon'
import qs from 'qs'

import {
  fetchServicesFlatResponse,
  providerQueryResponse,
  sendRequestOKResponse,
} from './fixtures'

import reducers, {
  //  getPostnummerDataAction,
  getProviders,
  setPageAction,
  //  setPostnummer,
  setField,
  setPage,
  select,
  setApiUrl,
  selectMany,
  sendRequestAction,
} from './store'

describe('store', function () {
  let store
  let mocks

  afterEach(function () {
    fetchMock.restore()
  })

  function mockPostnumberCheck() {
    mocks = fetchMock.mock(/.+frontendLookups\/checkPostcode\?postnummer/, {
      poststed: 'Oslo',
      hasProviders: true,
      providerCount: '10',
      activeKommune: true,
    })
    mocks.mock(
      /.+\/schema\/fetchServicesFlat\?postnummer=0575.*/,
      fetchServicesFlatResponse()
    )
  }
  function mockEvents() {
    mocks.mock(/\/events/, {
      status: 200,
      body: '',
    })
  }

  function mockPostRequest() {
    mocks.mock(/schema\/create.json/, {
      status: 200,
      body: JSON.stringify({}),
    })
  }
  beforeEach(function () {
    store = createStore(reducers, {}, applyMiddleware(thunk))
    setApiUrl('http://tesing.oljefri')
  })

  describe('setField', function () {
    it('should update state', function () {
      store.dispatch(setField('pn', 9010))
      expect(store.getState().fields).toEqual({
        pn: 9010,
      })
    })
  })

  describe('sendRequestAction', function () {
    it.skip('should save the request to the backend', function () {
      mockPostnumberCheck()
      mockEvents()
      mockPostRequest()
      store.dispatch(select('field', 9010))

      store.dispatch(sendRequestAction())
    })
  })

  describe('selectReducer', function () {
    it('handles unset fields', function () {
      let state
      store.dispatch(select('field', 9010))
      state = store.getState()

      expect(state.select.field[9010]).toBe(true)

      store.dispatch(select('field', 9010))
      state = store.getState()
      expect(state.select.field[9010]).toBe(false)

      store.dispatch(select('field', 9010))
      state = store.getState()
      expect(state.select.field[9010]).toBe(true)

      store.dispatch(selectMany('field', [9010, 9011], true))
      state = store.getState()
      expect(state.select.field[9010]).toEqual(true)
      expect(state.select.field[9011]).toBe(true)
    })
  })

  describe('setPage2', function () {
    beforeEach(function () {
      mockPostnumberCheck()
      mockEvents()
    })
    it('should check for postnummer status', function () {
      store.dispatch(setField('postnummer', '0575'))
      let state = store.getState()
      expect(state.fields.postnummer).toEqual('0575')

      let promise = store.dispatch(setPageAction(2))
      return promise.then(() => {
        state = store.getState()
        expect(state.postnummer).toEqual({
          hasProviders: true,
          providerCount: 10,
          poststed: 'Oslo',
          activeKommune: true,
        })
        expect(state.serviceGroups.groups.length).toBeGreaterThan(4)
        expect(state.page).toEqual(2)

        expect(state.fields.postPostplace).toEqual('Oslo')
        expect(state.fields.postPostcode).toEqual('0575')
      })
    })
  })

  describe('getProviders', function () {
    beforeEach(function () {
      mocks = fetchMock
        .mock(/\/schema\/providerQuery\?.+/, {
          status: 200,
          body: providerQueryResponse(),
        })
        .catch(404)

      const fields = {
        postnummer: '9010',
        buildingType_id: '2',
        yearBuilt: '1920',
        area: '150',
        comment: 'comment',
      }

      Object.keys(fields).forEach((key) => {
        store.dispatch(setField(key, fields[key]))
      })
      store.dispatch(select('service', 23))
    })

    it('should pass inn services added', function () {
      return store.dispatch(getProviders()).then((res) => {
        let lastUrl = fetchMock.lastUrl(/\/schema\/providerQuery\?.+/)
        expect(typeof lastUrl).toBe('string')
        expect(lastUrl).toContain('http://tesing.oljefri/')

        let query = qs.parse(lastUrl.substring(lastUrl.indexOf('?') + 1))

        expect(query.serviceTypes).toContain('23')
      })
    })
  })

  describe('setPage3', function () {
    beforeEach(function () {
      mocks = fetchMock
        .mock(/\/schema\/providerQuery\?.+/, {
          status: 200,
          body: providerQueryResponse(),
        })
        .catch(404)
      mockEvents()
      //      mocks.mock(/.*/, providerQueryResponse())
      const fields = {
        postnummer: '9010',
        buildingType_id: '2',
        yearBuilt: '1920',
        area: '150',
        comment: 'comment',
      }

      Object.keys(fields).forEach((key) => {
        store.dispatch(setField(key, fields[key]))
      })
    })

    it('setPage should handle strings', function () {
      store.dispatch(setPageAction('3'))
      expect(store.getState().page).toEqual(3)
    })

    it('should change to page 3 and get nr of possible providers', function () {
      store.dispatch(setPage(3))
      let state = store.getState()
      expect(state.page).toEqual(3)
    })

    it('should change to page 3 and get providers', function () {
      return store.dispatch(setPageAction(3)).then(() => {
        let state = store.getState()
        expect(state.page).toEqual(3)

        let lastUrl = fetchMock.lastUrl(/\/schema\/providerQuery\?.+/)

        expect(typeof lastUrl).toBe('string')
        expect(lastUrl).toContain('http://tesing.oljefri/')

        let query = qs.parse(lastUrl.substring(lastUrl.indexOf('?') + 1))

        expect(query.format).toEqual('json')
        //        expect(query.area).not.toBeDefined()
        //      expect(query.buildingType).not.toBeDefined()
        expect(query.postnummer).toEqual('9010')
        //    expect(query.market).not.toBeDefined()
        expect(query.serviceTypes).not.toBeDefined()
      })
    })
  })

  describe('setPage4', function () {
    beforeEach(function () {
      mocks = fetchMock.mock(/\/schema\/providerQuery\?/, {
        status: 200,
        body: providerQueryResponse(),
      })
      mockEvents()

      //      mocks.mock(/.*/, providerQueryResponse())
      const fields = {
        postnummer: '9010',
        buildingType_id: '2',
        yearBuilt: '1920',
        area: '150',
        comment: 'comment',
      }

      Object.keys(fields).forEach((key) => {
        store.dispatch(setField(key, fields[key]))
      })
    })
    it('should change to page 4 and get providers', function () {
      return store.dispatch(setPageAction(4)).then(() => {
        let state = store.getState()
        expect(state.page).toEqual(4)

        let lastUrl = fetchMock.lastUrl(/\/schema\/providerQuery\?/)
        expect(lastUrl).not.toBe(null)

        let query = qs.parse(lastUrl.substring(lastUrl.indexOf('?') + 1))

        expect(query.format).toEqual('json')
        expect(query.area).toEqual('150')
        expect(query.buildingType).toEqual('2')
        expect(query.postnummer).toEqual('9010')
        expect(query.market).toEqual('private')
        expect(query.serviceTypes).not.toBeDefined()
      })
    })
  })

  describe('setPage5', function () {
    it('should change to page 5', function () {
      store.dispatch(setPage(5))
      let state = store.getState()
      expect(state.page).toEqual(5)
    })
  })

  describe('setPage6', function () {
    beforeEach(function () {
      store = createStore(
        reducers,
        {
          select: {
            selectedProvider: {
              45: true,
            },
            service: {
              14: false,
              13: true,
            },
          },
          fields: {
            postnummer: '9010',
            postPostcode: '9010',
            postPostplace: 'Tromsø',
            buildingType_id: '2',
            yearBuilt: '1920',
            area: '90',
            comment: 'Kommentar',
            firstname: 'Kaja',
            lastname: 'Nordby',
            postAddress: 'Godthåpveien 11',
            phone: '977 553381',
            email: 'kaja@gti.com',
            termsAgreed: true,
            wantsToBeMember: true,
          },
        },
        applyMiddleware(thunk)
      )
    })
    it('should post the data', function () {
      mocks = fetchMock.mock(/\/schema\/create.json/, {
        status: 200,
        body: sendRequestOKResponse(),
      })
      mockEvents()

      return store.dispatch(setPageAction(6)).then(() => {
        let state = store.getState()
        expect(state.page).toEqual(6)

        let lastCall = fetchMock.lastCall(/\/schema\/create.json/)
        expect(lastCall).toBeDefined()

        let data = JSON.parse(lastCall[1].body)
        expect(data.requester).toEqual({
          firstname: 'Kaja',
          lastname: 'Nordby',
          postAddress: 'Godthåpveien 11',
          postPostcode: '9010',
          phone: '977 553381',
          email: 'kaja@gti.com',
          email_again: 'kaja@gti.com',
          termsAgreed: true,
        })
        expect(data.buildingdescription_new).toEqual({
          area: '90',
          buildingtype_id: '2',
          yearBuilt: '1920',
        })
        expect(data.services_list).toEqual(['13'])
        expect(data.comment).toEqual('Kommentar')
        expect(data.requestSite).toEqual('http://localhost/')

        expect(data.providersSelected).toEqual(['45'])

        expect(state.requestResult).toBeDefined()

        expect(state.requestResult).toEqual(sendRequestOKResponse())
      })
    })
  })
})
