const buildingTypes = [{
    'id': '1',
    'name': 'Annen',
    'private': true,
    'business': true,
    'meanEnergyUsage': '487'
  },
  { 'id': '2', 'name': 'Enebolig', 'private': true, 'business': false, 'meanEnergyUsage': '200' },
//  { 'id': '3', 'name': 'Lager', 'private': false, 'business': true, 'meanEnergyUsage': '402' },
  { 'id': '4', 'name': 'Leilighet', 'private': true, 'business': false, 'meanEnergyUsage': '170' },
//  { 'id': '5', 'name': 'Skal ikke dukke opp', 'private': null, 'business': null, 'meanEnergyUsage': null },
  { 'id': '6', 'name': 'Rekkehus/tomannsbolig', 'private': true, 'business': false, 'meanEnergyUsage': '175' },
 // { 'id': '7', 'name': 'Kontorbygg', 'private': false, 'business': true, 'meanEnergyUsage': '283' },
 // { 'id': '8', 'name': 'Butikklokaler', 'private': false, 'business': true, 'meanEnergyUsage': '487' },
 // { 'id': '9', 'name': 'Industri', 'private': false, 'business': true, 'meanEnergyUsage': '324' },
  { 'id': '10', 'name': 'Borettslag/Næringsbygg', 'private': false, 'business': true, 'meanEnergyUsage': '248' }
]
export default buildingTypes
