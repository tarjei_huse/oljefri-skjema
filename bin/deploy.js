const fs = require('fs-extra')
const path = require('path')
const manifest = require('../build/asset-manifest.json')

const deploy = process.env.DEPLOY_DIR

if (!manifest) {
  throw new Error('Manifest not found!')
}

if (!deploy) {
  throw new Error('DEPLOY_DIR not defined!' + deploy)
}

const { files } = manifest

const jsFile = path.resolve(deploy, files['main.js'])

const loadFileKeys = Object.keys(manifest.files).filter((key) => {
  if (key === 'main.js') {
    return true
  }
  if (key.indexOf('chunk.js') > -1 && key.indexOf('.js.map') === -1) {
    return true
  }
  return false
})

console.log('loadFileKeys', loadFileKeys)
const loadFiles = loadFileKeys.map((key) => files[key])

console.log('loadFiles', loadFiles)

const runtime = fs.readFileSync(path.resolve(deploy, files['runtime-main.js']))

const jsSchema = `
(function oljefri_admin_loadStyle() {
  var head = document.getElementsByTagName('head')[0]
  var link = document.createElement('link')
  link.href = 'https://admin.oljefri.no/${files['main.css']}';
  link.type = 'text/css'
  link.rel = 'stylesheet'
  head.appendChild(link)
})();

${runtime}

function loadJS(url, implementationCode, location){
    //url is URL of external file, implementationCode is the code
    //to be called from the file, location is the location to
    //insert the <script> element

    var scriptTag = document.createElement('script');
    scriptTag.src = url;

    scriptTag.onload = implementationCode;
    scriptTag.onreadystatechange = implementationCode;

    location.appendChild(scriptTag);
};

var files = ${JSON.stringify(loadFiles)};

for (var i = 0; i < files.length; i++) {
  loadJS('https://admin.oljefri.no/' + files[i], function() {}, document.body);
}
`

const jsSchemaFileOld = path.resolve(deploy, '_js_schema.php')
const jsSchemaFile = path.resolve(deploy, '_js_schema_1.php')
fs.outputFileSync(jsSchemaFile, jsSchema)
fs.outputFileSync(jsSchemaFileOld, jsSchema)

const jsDir = path.dirname(files['main.js'])
let deployHost = 'oljeadm@oljefri.no'
let deployPath = `/home/oljeadm/symfony/web/`
deployHost = 'tarjei@d9.novap.no'
deployPath = `/web/vhosts/admin.energismart.no/shared/public/`

console.log('JSDIR: ', jsDir)

let bashScript = `#!/bin/bash
set -x
ssh ${deployHost} mkdir -p ${deployPath}${jsDir}/
ssh ${deployHost} mkdir -p ${deployPath}${jsDir}/../css
scp ${jsSchemaFile} ${deployHost}:${deployPath}
scp ${jsSchemaFileOld} ${deployHost}:${deployPath}

`
;['main.js', 'main.css'].forEach((key) => {
  const file = path.resolve(deploy, files[key])
  const jsDir = path.dirname(files[key])
  bashScript += '\n# cp script: ' + key + '\n'
  bashScript += `\nscp ${file} ${deployHost}:${deployPath}${jsDir}/`
  bashScript += `\nscp ${file}.map ${deployHost}:${deployPath}${jsDir}/`
})

bashScript += '\n'

Object.values(files).forEach((fileName) => {
  let file = path.resolve(deploy, fileName)

  bashScript += `\nscp ${file} ${deployHost}:${deployPath}${jsDir}/`
})

bashScript += '\n\n'

console.log(bashScript)

fs.outputFileSync(
  path.resolve(__dirname, '..', 'build', 'deploy-to-server.sh'),
  bashScript,
  { mode: 0o755 }
)
